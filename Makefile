.PHONY: build-test
build-test:
	go build -o bin/test cmd/main.go

.PHONY: check
check: fmt vet golangci-lint

.PHONY: go-test
test:
	go test -count=1 -coverprofile cover.out $(shell go list ./... | grep -v cksv1beta1) && go tool cover -func cover.out

.PHONY: fmt
fmt: ## Run go fmt against code.
	go fmt ./...

.PHONY: vet
vet: ## Run go vet against code.
	go vet ./...

.PHPNY: golangci-lint
golangci-lint:
ifeq ("$(shell golangci-lint version 2>/dev/null)", "")
	go install github.com/golangci/golangci-lint/cmd/golangci-lint@v1.44.0
endif
	golangci-lint run ./... --timeout 3m