[![Go Report Card](https://goreportcard.com/badge/gitlab.com/kindaicvlab/cvcloud/cvcloud)](https://goreportcard.com/report/gitlab.com/kindaicvlab/cvcloud/cvcloud)
[![Coverage](https://gitlab.com/kindaicvlab/cvcloud/cvcloud/badges/master/coverage.svg)](https://gitlab.com/kindaicvlab/cvcloud/cvcloud/-/commits/master)

# Client Tool for CVCloud

![](docs/terminalizer/cvcloud.gif)
# ユーザガイド

詳細な使用方法は [ユーザガイド](./docs/userguide.md)，既知の問題は，[known issues](docs/knownissues.md) を参照してください．

```shell
$ cvcloud help

Client Tool for CVCloud

Usage:
  cvcloud [command]

Available Commands:
  completion  generate the autocompletion script for the specified shell
  create      create applications or storages
  delete      delete your applications or storages
  get         get machines, applications, storages or available container images
  help        Help about any command
  init        initialize cvcloud
  update      update capacity size of your created storages
  version     show cvcloud version

Flags:
  -h, --help   help for cvcloud

Use "cvcloud [command] --help" for more information about a command.
```

# CVCloud とは

CVLAB. Kontainer Service (cks) によって提供されているインフラストラクチャ上に機械学習用コーディング & 学習用環境を構築し，ユーザへ提供するためのクライアントツールです．

cvcloud コマンドは `cks-operator`・`imperator` と連携し，動作しているため実際にアプリケーションを提供しているのは `cks-operator`で，
コンピュートリソースを管理しているのは `imperator` です．

![CVCloud](./imgs/cvcloud.svg)
