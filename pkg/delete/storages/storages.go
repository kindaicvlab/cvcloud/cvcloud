/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package storages

import (
	"fmt"
	"io"
	"strings"

	"github.com/spf13/cobra"
	cksoperatorv1beta2 "gitlab.com/kindaicvlab/cvcloud/cks-operator/api/v1beta2"
	cksoperatorconsts "gitlab.com/kindaicvlab/cvcloud/cks-operator/consts"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/consts"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/kubectl"
)

type deleteStorageOptions struct{}

const deleteStorageDescription = `
The command deletes your created storages.
`

func NewDeleteStorageCmd(client *kubectl.Client, stdOut io.Writer) *cobra.Command {

	o := &deleteStorageOptions{}

	cmd := &cobra.Command{
		Use:          "storages",
		Aliases:      consts.StoragesAlias,
		Short:        "delete your created storages",
		Long:         deleteStorageDescription,
		SilenceUsage: true,
		RunE: func(cmd *cobra.Command, args []string) error {
			return o.run(client, stdOut)
		},
	}

	return cmd
}

func (o *deleteStorageOptions) run(client *kubectl.Client, stdOut io.Writer) error {

	// search pods
	if err := searchRunningPods(client, map[string]string{
		cksoperatorconsts.CksAppTypeLabelKey: cksoperatorv1beta2.CodeServer.String(),
	}); err != nil {
		return err
	}
	if err := searchRunningPods(client, map[string]string{
		cksoperatorconsts.CksAppTypeLabelKey: cksoperatorv1beta2.JupyterLab.String(),
	}); err != nil {
		return err
	}

	// delete CronJob, Job, ConfigMap
	if err := deleteBackUpComponents(client); err != nil {
		return err
	}

	// delete cephfs pvc
	if err := deletePVC(client, strings.Join([]string{client.GetNamespace(), "pvc"}, "-")); err != nil {
		return err
	}

	// delete nfs pvc
	latestNFSPVC, err := client.GetLatestPVCByNFSProvisioner()
	if err != nil {
		return err
	}
	if err := deletePVC(client, latestNFSPVC.Name); err != nil {
		return err
	}

	if _, err := fmt.Fprintln(stdOut, "Request Successfully!"); err != nil {
		return err
	}

	return nil
}

func searchRunningPods(client *kubectl.Client, label map[string]string) error {

	var pods corev1.PodList
	if err := client.ListResources(&pods, label); err != nil {
		return err
	}
	if len(pods.Items) > 0 {
		return fmt.Errorf("applications exist: remove applications before removing storages")
	}
	return nil

}

func deletePVC(client *kubectl.Client, PVCName string) error {

	var pvc corev1.PersistentVolumeClaim
	if err := client.GetResources(&pvc, PVCName); err != nil {
		return err
	}
	if err := client.DeleteResources(&pvc); err != nil {
		return err
	}

	return nil
}

func deleteBackUpComponents(client *kubectl.Client) error {
	cj := &batchv1.CronJob{}
	if err := client.GetResources(cj, strings.Join([]string{client.GetNamespace(), "pvc", consts.FullBackup}, "-")); err != nil {
		return err
	}
	if err := client.DeleteResources(cj); err != nil {
		return err
	}

	var cm corev1.ConfigMap
	if err := client.GetResources(&cm, cksoperatorconsts.BackUpConfigMapName); err != nil {
		return err
	}
	if err := client.DeleteResources(&cm); err != nil {
		return err
	}

	return nil
}
