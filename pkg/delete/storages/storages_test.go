/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package storages

import (
	"os"
	"strings"
	"testing"
	"time"

	cksoperatorv1beta2 "gitlab.com/kindaicvlab/cvcloud/cks-operator/api/v1beta2"
	cksoperatorconsts "gitlab.com/kindaicvlab/cvcloud/cks-operator/consts"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/consts"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/testutils"
)

func TestDeleteStorages(t *testing.T) {

	rookCephFSCSI := "rook-cephfs"
	cephfs := &corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name:      strings.Join([]string{testutils.TestNameSpace, "pvc"}, "-"),
			Namespace: testutils.TestNameSpace,
			Labels: map[string]string{
				cksoperatorconsts.CksStorageTypeLabelKey: cksoperatorconsts.CephFsLabel,
			},
		},
		Spec: corev1.PersistentVolumeClaimSpec{
			AccessModes: []corev1.PersistentVolumeAccessMode{
				corev1.ReadWriteMany,
			},
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					corev1.ResourceStorage: resource.MustParse(strings.Join([]string{
						testutils.TestStorageSize,
						"Gi",
					}, "")),
				},
			},
			StorageClassName: &rookCephFSCSI,
		},
		Status: corev1.PersistentVolumeClaimStatus{
			Phase: corev1.ClaimBound,
		},
	}

	codeServer := &corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name:      strings.Join([]string{cksoperatorv1beta2.CodeServer.String(), testutils.TestNameSpace}, "-"),
			Namespace: testutils.TestNameSpace,
			Labels: map[string]string{
				cksoperatorconsts.CksAppTypeLabelKey: cksoperatorv1beta2.CodeServer.String(),
			},
		},
	}

	jupyterLab := &corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name:      strings.Join([]string{cksoperatorv1beta2.JupyterLab.String(), testutils.TestNameSpace}, "-"),
			Namespace: testutils.TestNameSpace,
			Labels: map[string]string{
				cksoperatorconsts.CksAppTypeLabelKey: cksoperatorv1beta2.JupyterLab.String(),
			},
		},
	}

	now := time.Now()
	layout := "20210105-1504"
	nfs := &corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name:      strings.Join([]string{testutils.TestNameSpace, cksoperatorconsts.NfsLabel, now.Format(layout)}, "-"),
			Namespace: testutils.TestNameSpace,
			Labels: map[string]string{
				cksoperatorconsts.CksStorageTypeLabelKey: cksoperatorconsts.NfsLabel,
			},
			Annotations: map[string]string{
				corev1.BetaStorageClassAnnotation: consts.NfsProvisionerName,
			},
		},
		Spec: corev1.PersistentVolumeClaimSpec{
			AccessModes: []corev1.PersistentVolumeAccessMode{
				corev1.ReadWriteMany,
			},
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					corev1.ResourceStorage: resource.MustParse(strings.Join([]string{
						testutils.TestStorageSize,
						"Gi",
					}, "")),
				},
			},
		},
		Status: corev1.PersistentVolumeClaimStatus{
			Phase: corev1.ClaimBound,
		},
	}

	fullbackUpCronJob := &batchv1.CronJob{
		ObjectMeta: metav1.ObjectMeta{
			Name:      strings.Join([]string{testutils.TestNameSpace, "pvc", consts.FullBackup}, "-"),
			Namespace: testutils.TestNameSpace,
			Labels: map[string]string{
				cksoperatorconsts.CksAppTypeLabelKey: consts.FullBackup,
			},
		},
	}

	fullbackUpJob := &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{
			Name:      strings.Join([]string{testutils.TestNameSpace, "pvc", consts.FullBackup}, "-"),
			Namespace: testutils.TestNameSpace,
			Labels: map[string]string{
				cksoperatorconsts.CksAppTypeLabelKey: consts.FullBackup,
			},
		},
	}

	backupConfigMap := &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cksoperatorconsts.BackUpConfigMapName,
			Namespace: testutils.TestNameSpace,
			Labels: map[string]string{
				cksoperatorconsts.CksAppTypeLabelKey: consts.FullBackup,
			},
		},
	}

	testCases := []struct {
		name          string
		wantError     bool
		kubeResources []client.Object
	}{
		{
			name:          "all resources is ok",
			wantError:     false,
			kubeResources: []client.Object{cephfs, nfs, fullbackUpCronJob, backupConfigMap},
		},
		{
			name:          "all resources including Job is ok",
			wantError:     false,
			kubeResources: []client.Object{cephfs, nfs, fullbackUpCronJob, fullbackUpJob, backupConfigMap},
		},
		{
			name:          "cephfs was not found",
			wantError:     true,
			kubeResources: []client.Object{nfs, fullbackUpCronJob, backupConfigMap},
		},
		{
			name:          "nfs was not found",
			wantError:     true,
			kubeResources: []client.Object{cephfs, fullbackUpCronJob, backupConfigMap},
		},
		{
			name:          "fullbackup was not found",
			wantError:     true,
			kubeResources: []client.Object{cephfs, nfs, backupConfigMap},
		},
		{
			name:          "backupConfigMap was not found",
			wantError:     true,
			kubeResources: []client.Object{cephfs, nfs, fullbackUpCronJob},
		},
		{
			name:          "running code-server pods",
			wantError:     true,
			kubeResources: []client.Object{codeServer, cephfs, nfs, fullbackUpCronJob, backupConfigMap},
		},
		{
			name:          "running jupyterlab pods",
			wantError:     true,
			kubeResources: []client.Object{jupyterLab, cephfs, nfs, fullbackUpCronJob, backupConfigMap},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			c, err := testutils.NewFakeKubeClient(test.kubeResources)
			if err != nil {
				t.Error(err)
			}
			err = NewDeleteStorageCmd(c, os.Stdout).Execute()
			if (err != nil) != test.wantError {
				t.Errorf("expected error: %v, got '%v'\n", test.wantError, err)
			}
		})
	}

}
