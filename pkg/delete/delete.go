/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package delete

import (
	"io"

	"github.com/spf13/cobra"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/delete/applications"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/delete/storages"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/kubectl"
)

const deleteDescription = `
This command consists of multiple subcommand which can be used  to 
delete the created resources, including:

- The applications to run and edit your codes
- The storages to save your data
`

func NewDeleteCmd(stdOut io.Writer) *cobra.Command {

	cmd := &cobra.Command{
		Use:          "delete",
		Short:        "delete your applications or storages",
		Long:         deleteDescription,
		SilenceUsage: true,
	}

	client := &kubectl.Client{}
	cmd.AddCommand(applications.NewDeleteAppCmd(client, stdOut))
	cmd.AddCommand(storages.NewDeleteStorageCmd(client, stdOut))

	return cmd
}
