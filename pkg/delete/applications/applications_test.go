/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package applications

import (
	"os"
	"strings"
	"testing"

	cksoperatorv1beta2 "gitlab.com/kindaicvlab/cvcloud/cks-operator/api/v1beta2"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/testutils"
)

func TestDeleteApplications(t *testing.T) {

	codeServer := &cksoperatorv1beta2.MachineLearning{
		TypeMeta: metav1.TypeMeta{
			APIVersion: cksoperatorv1beta2.GroupVersion.String(),
			Kind:       "MachineLearning",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: strings.Join([]string{
				cksoperatorv1beta2.CodeServer.String(),
				testutils.TestNameSpace,
			}, "-"),
			Namespace: testutils.TestNameSpace,
		},
		Spec: cksoperatorv1beta2.MachineLearningSpec{
			MachineLearningApps: []cksoperatorv1beta2.MachineLearningApps{
				{
					MLAppSpec: cksoperatorv1beta2.MLAppSpec{
						Type: cksoperatorv1beta2.CodeServer,
					},
				},
			},
		},
	}

	jupyterLab := &cksoperatorv1beta2.MachineLearning{
		TypeMeta: metav1.TypeMeta{
			APIVersion: cksoperatorv1beta2.GroupVersion.String(),
			Kind:       "MachineLearning",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: strings.Join([]string{
				cksoperatorv1beta2.JupyterLab.String(),
				testutils.TestNameSpace,
			}, "-"),
			Namespace: testutils.TestNameSpace,
		},
		Spec: cksoperatorv1beta2.MachineLearningSpec{
			MachineLearningApps: []cksoperatorv1beta2.MachineLearningApps{
				{
					MLAppSpec: cksoperatorv1beta2.MLAppSpec{
						Type: cksoperatorv1beta2.JupyterLab,
					},
				},
			},
		},
	}

	testCases := []struct {
		name          string
		wantError     bool
		flags         map[string]string
		kubeResources []client.Object
	}{
		{
			name:          "delete code-server applications successfully",
			flags:         map[string]string{"type": cksoperatorv1beta2.CodeServer.String()},
			wantError:     false,
			kubeResources: []client.Object{codeServer},
		},
		{
			name:          "specify type is code-server, running application type is jupyterlab",
			flags:         map[string]string{"type": cksoperatorv1beta2.CodeServer.String()},
			wantError:     true,
			kubeResources: []client.Object{jupyterLab},
		},
		{
			name:          "delete code-server applications successfully",
			flags:         map[string]string{"type": cksoperatorv1beta2.JupyterLab.String()},
			wantError:     false,
			kubeResources: []client.Object{jupyterLab},
		},
		{
			name:          "specify type is jupyterlab, running application type is code-server",
			flags:         map[string]string{"type": cksoperatorv1beta2.JupyterLab.String()},
			wantError:     true,
			kubeResources: []client.Object{codeServer},
		},
		{
			name:          "delete code-server applications successfully when not specifying type option",
			wantError:     false,
			kubeResources: []client.Object{codeServer},
		},
		{
			name:      "there are not any machinelearning resources",
			wantError: true,
		},
		{
			name:          "there are two machinelearning resources",
			wantError:     true,
			kubeResources: []client.Object{codeServer, jupyterLab},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			c, err := testutils.NewFakeKubeClient(test.kubeResources)
			if err != nil {
				t.Error(err)
			}
			command := NewDeleteAppCmd(c, os.Stdout)
			err = testutils.StartFakeCommandWithOptions(test.flags, make([]string, 0), os.Stdout, command)

			if (err != nil) != test.wantError {
				t.Errorf("expected error: %v, got '%v'\n", test.wantError, err)
			}
		})
	}

}
