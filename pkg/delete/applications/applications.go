/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package applications

import (
	"fmt"
	"io"
	"log"

	"github.com/spf13/cobra"
	cksoperatorv1beta2 "gitlab.com/kindaicvlab/cvcloud/cks-operator/api/v1beta2"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/consts"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/kubectl"
)

type deleteAppOptions struct {
	appType string
}

const deleteAppDescription = `
The command deletes your created applications.
`

func NewDeleteAppCmd(client *kubectl.Client, stdOut io.Writer) *cobra.Command {

	o := &deleteAppOptions{}

	cmd := &cobra.Command{
		Use:          "applications",
		Aliases:      consts.ApplicationsAlias,
		Short:        "delete your created applications",
		Long:         deleteAppDescription,
		SilenceUsage: true,
		RunE: func(cmd *cobra.Command, args []string) error {
			return o.run(client, stdOut)
		},
	}

	f := cmd.Flags()
	f.StringVarP(&o.appType, "type", "t", "", "set app type, jupyterlab or codeserver")
	if err := cmd.RegisterFlagCompletionFunc("type", func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		return []string{cksoperatorv1beta2.JupyterLab.String(), cksoperatorv1beta2.CodeServer.String()}, cobra.ShellCompDirectiveDefault
	}); err != nil {
		log.Fatal(err)
	}

	return cmd
}

func (o *deleteAppOptions) run(client *kubectl.Client, stdOut io.Writer) error {

	machineLearnings := &cksoperatorv1beta2.MachineLearningList{}
	if err := client.ListResources(machineLearnings, map[string]string{}); err != nil {
		return err
	}

	if len(machineLearnings.Items) == 0 {
		return fmt.Errorf("your applications was not found")
	} else if len(machineLearnings.Items) >= 2 {
		return fmt.Errorf("there are '%d' applications, take a moment and try again", len(machineLearnings.Items))
	}

	var application = machineLearnings.Items[0]
	switch cksoperatorv1beta2.ApplicationType(o.appType) {
	case cksoperatorv1beta2.CodeServer:
		if application.Spec.MachineLearningApps[0].MLAppSpec.Type != cksoperatorv1beta2.CodeServer {
			return fmt.Errorf("'%s' application does not found", cksoperatorv1beta2.CodeServer)
		}
		if err := client.DeleteResources(&application); err != nil {
			return err
		}
	case cksoperatorv1beta2.JupyterLab:
		if application.Spec.MachineLearningApps[0].MLAppSpec.Type != cksoperatorv1beta2.JupyterLab {
			return fmt.Errorf("'%s' application does not found", cksoperatorv1beta2.JupyterLab)
		}
		if err := client.DeleteResources(&application); err != nil {
			return err
		}
	default:
		if err := client.DeleteResources(&application); err != nil {
			return err
		}
	}

	if _, err := fmt.Fprintln(stdOut, "Request Successfully!"); err != nil {
		return err
	}

	return nil

}
