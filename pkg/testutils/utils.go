/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package testutils

import (
	"io"
	"os"

	"github.com/spf13/cobra"
	imperatorv1alpha1 "github.com/tenzen-y/imperator/pkg/api/v1alpha1"
	cksoperatorv1beta2 "gitlab.com/kindaicvlab/cvcloud/cks-operator/api/v1beta2"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/kubernetes/scheme"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/kubectl"
)

func NewFakeKubeClient(kubeResources []client.Object) (*kubectl.Client, error) {
	scm := runtime.NewScheme()
	if err := scheme.AddToScheme(scm); err != nil {
		return &kubectl.Client{}, err
	}
	if err := cksoperatorv1beta2.AddToScheme(scm); err != nil {
		return nil, err
	}
	if err := imperatorv1alpha1.AddToScheme(scm); err != nil {
		return nil, err
	}

	fakeClientBuilder := fake.
		NewClientBuilder().
		WithScheme(scm)

	if len(kubeResources) > 0 {
		for _, kr := range kubeResources {
			fakeClientBuilder.WithObjects(kr)
		}
		namespace := corev1.Namespace{
			ObjectMeta: metav1.ObjectMeta{
				Name: TestNameSpace,
			},
		}
		fakeClientBuilder.WithObjects(&namespace)
	}

	return &kubectl.Client{
		KubeClient:    fakeClientBuilder.Build(),
		UserNameSpace: TestNameSpace,
	}, nil
}

func StartFakeCommandWithOptions(flagList map[string]string, args []string, out io.Writer, command *cobra.Command) error {
	command.SetOut(out)
	oldStdin := os.Stdin
	if len(flagList) != 0 {
		for name, value := range flagList {
			if err := command.Flags().Set(name, value); err != nil {
				return err
			}
		}
	}
	if len(args) != 0 {
		command.SetArgs(args)
	}
	err := command.Execute()
	os.Stdin = oldStdin

	return err
}
