/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package create

import (
	"io"

	"github.com/spf13/cobra"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/create/applications"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/create/storages"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/kubectl"
)

const createDescription = `
This command consists of multiple subcommands which can be used
create the resources provided cvcloud, including:

- The applications to run and edit your codes
- The storages to save your data
`

func NewCreateCmd(stdOut io.Writer) *cobra.Command {

	cmd := &cobra.Command{
		Use:          "create",
		Short:        "create applications or storages",
		Long:         createDescription,
		SilenceUsage: true,
	}

	client := &kubectl.Client{}
	cmd.AddCommand(applications.NewCreateAppCmd(client, stdOut))
	cmd.AddCommand(storages.NewCreateStorageCmd(client, stdOut))

	return cmd
}
