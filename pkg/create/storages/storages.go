/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package storages

import (
	"fmt"
	"io"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/consts"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/kubectl"
)

type createStorageOptions struct {
	size int16
}

const createStorageDescription = `
The command creates storages to save your data
`

func NewCreateStorageCmd(client *kubectl.Client, stdOut io.Writer) *cobra.Command {

	o := &createStorageOptions{}

	cmd := &cobra.Command{
		Use:          "storages",
		Aliases:      consts.StoragesAlias,
		Short:        "create storages to save your data",
		Long:         createStorageDescription,
		SilenceUsage: true,
		RunE: func(cmd *cobra.Command, args []string) error {
			return o.run(client, stdOut)
		},
	}

	f := cmd.Flags()
	f.Int16VarP(&o.size, "size", "s", consts.DefaultVolumeSize, "require persistent volume size")

	return cmd
}

func (o *createStorageOptions) run(client *kubectl.Client, stdOut io.Writer) error {

	if o.size > consts.MaxUserStorageSize/4 {
		return fmt.Errorf("storage size must be '%d' or less", int(consts.MaxUserStorageSize/4))
	}

	var pvc corev1.PersistentVolumeClaim
	if err := client.GetCephFSPVC(&pvc); err == nil {
		return fmt.Errorf("your storages already exists")
	}

	// storage
	var (
		cephFS corev1.PersistentVolumeClaim
		nfs    corev1.PersistentVolumeClaim
		// create cephfs pvc & nfs pvc
		requiredVolume = strings.Join([]string{strconv.Itoa(int(o.size)), "Gi"}, "")
	)

	if err := client.CreateCephFS(&cephFS, requiredVolume); err != nil {
		return err
	}
	if err := client.CreateNFS(&nfs, requiredVolume); err != nil {
		return err
	}

	// backup components
	var (
		config  corev1.ConfigMap
		cronJob batchv1.CronJob
	)

	// create configmap
	if err := client.CreateConfigMap(&config); err != nil {
		return err
	}
	// create fullbackup cronjob
	if err := client.CreateCronJob(&cronJob); err != nil {
		return err
	}

	if _, err := fmt.Fprintln(stdOut, "Request Successfully!"); err != nil {
		return err
	}

	return nil
}
