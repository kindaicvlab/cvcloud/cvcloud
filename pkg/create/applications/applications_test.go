/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package applications

import (
	"context"
	"os"
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
	imperatorv1alpha1 "github.com/tenzen-y/imperator/pkg/api/v1alpha1"
	cksoperatorv1beta2 "gitlab.com/kindaicvlab/cvcloud/cks-operator/api/v1beta2"
	cksoperatorconsts "gitlab.com/kindaicvlab/cvcloud/cks-operator/consts"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/consts"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/image"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/testutils"
)

func TestCreateApplications(t *testing.T) {

	const (
		existImageName    = "cuda11.1.1-cudnn8"
		notExistImageName = "cuda9.0"
	)

	var (
		deletionTime = metav1.Now()
	)

	machineLearning := &cksoperatorv1beta2.MachineLearning{
		TypeMeta: metav1.TypeMeta{
			APIVersion: cksoperatorv1beta2.GroupVersion.String(),
			Kind:       "MachineLearning",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: strings.Join([]string{
				"machinelearning",
				testutils.TestNameSpace,
			}, "-"),
			Namespace: testutils.TestNameSpace,
		},
	}

	availableMachine := &imperatorv1alpha1.Machine{
		TypeMeta: metav1.TypeMeta{
			APIVersion: imperatorv1alpha1.GroupVersion.String(),
			Kind:       "Machine",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: consts.MachineResourceName,
		},
		Status: imperatorv1alpha1.MachineStatus{
			AvailableMachines: []imperatorv1alpha1.AvailableMachineCondition{
				{
					Name: testutils.TestMachineName,
					Usage: imperatorv1alpha1.UsageCondition{
						Maximum:  2,
						Reserved: 2,
						Used:     0,
						Waiting:  0,
					},
				},
			},
		},
	}

	unavailableMachine := &imperatorv1alpha1.Machine{
		TypeMeta: metav1.TypeMeta{
			APIVersion: imperatorv1alpha1.GroupVersion.String(),
			Kind:       "Machine",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: consts.MachineResourceName,
		},
		Status: imperatorv1alpha1.MachineStatus{
			AvailableMachines: []imperatorv1alpha1.AvailableMachineCondition{
				{
					Name: testutils.TestMachineName,
					Usage: imperatorv1alpha1.UsageCondition{
						Maximum:  2,
						Reserved: 0,
						Used:     2,
						Waiting:  0,
					},
				},
			},
		},
	}

	codeServer := &corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name:      strings.Join([]string{cksoperatorv1beta2.CodeServer.String(), testutils.TestNameSpace}, "-"),
			Namespace: testutils.TestNameSpace,
			Labels: map[string]string{
				cksoperatorconsts.CksAppTypeLabelKey: cksoperatorv1beta2.CodeServer.String(),
			},
		},
	}

	terminatingCodeServer := &corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name:      strings.Join([]string{cksoperatorv1beta2.CodeServer.String(), testutils.TestNameSpace}, "-"),
			Namespace: testutils.TestNameSpace,
			Labels: map[string]string{
				cksoperatorconsts.CksAppTypeLabelKey: cksoperatorv1beta2.CodeServer.String(),
			},
			DeletionTimestamp: &deletionTime,
		},
	}

	jupyterLab := &corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name:      strings.Join([]string{cksoperatorv1beta2.JupyterLab.String(), testutils.TestNameSpace}, "-"),
			Namespace: testutils.TestNameSpace,
			Labels: map[string]string{
				cksoperatorconsts.CksAppTypeLabelKey: cksoperatorv1beta2.JupyterLab.String(),
			},
		},
	}
	terminatingJupyterLab := &corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name:      strings.Join([]string{cksoperatorv1beta2.JupyterLab.String(), testutils.TestNameSpace}, "-"),
			Namespace: testutils.TestNameSpace,
			Labels: map[string]string{
				cksoperatorconsts.CksAppTypeLabelKey: cksoperatorv1beta2.JupyterLab.String(),
			},
			DeletionTimestamp: &deletionTime,
		},
	}

	rookCephFSCSI := "rook-cephfs"
	cephfs := &corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name:      strings.Join([]string{testutils.TestNameSpace, "pvc"}, "-"),
			Namespace: testutils.TestNameSpace,
			Labels: map[string]string{
				cksoperatorconsts.CksStorageTypeLabelKey: cksoperatorconsts.CephFsLabel,
			},
		},
		Spec: corev1.PersistentVolumeClaimSpec{
			AccessModes: []corev1.PersistentVolumeAccessMode{
				corev1.ReadWriteMany,
			},
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					corev1.ResourceStorage: resource.MustParse(strings.Join([]string{
						testutils.TestStorageSize,
						"Gi",
					}, "")),
				},
			},
			StorageClassName: &rookCephFSCSI,
		},
		Status: corev1.PersistentVolumeClaimStatus{
			Phase: corev1.ClaimBound,
		},
	}

	deployCodeServerTestCases := []struct {
		name          string
		image         string
		machine       string
		wantError     bool
		kubeResources []client.Object
	}{
		{
			name:          "create codeserver applications successfully when request type is code-server",
			image:         existImageName,
			machine:       testutils.TestMachineName,
			wantError:     false,
			kubeResources: []client.Object{availableMachine, cephfs},
		},
		{
			name:          "image was not found when request type is code-server",
			image:         notExistImageName,
			machine:       testutils.TestMachineName,
			wantError:     true,
			kubeResources: []client.Object{availableMachine},
		},
		{
			name:          "image was not found when request type is code-server by application alias subcommand",
			image:         notExistImageName,
			machine:       testutils.TestMachineName,
			wantError:     true,
			kubeResources: []client.Object{availableMachine},
		},
		{
			name:          "there are not available machine when request type is code-server",
			image:         existImageName,
			machine:       testutils.TestMachineName,
			wantError:     true,
			kubeResources: []client.Object{unavailableMachine, cephfs},
		},
		{
			name:          "cephFS was not found",
			image:         existImageName,
			machine:       testutils.TestMachineName,
			wantError:     true,
			kubeResources: []client.Object{availableMachine},
		},
		{
			name:          "running code-server pod and exist machinelearning when request type is code-server",
			image:         existImageName,
			machine:       testutils.TestMachineName,
			wantError:     true,
			kubeResources: []client.Object{availableMachine, cephfs, machineLearning, codeServer},
		},
		{
			name:          "running jupyterlab pod and exist machinelearning when request type is code-server",
			image:         existImageName,
			machine:       testutils.TestMachineName,
			wantError:     true,
			kubeResources: []client.Object{availableMachine, cephfs, machineLearning, jupyterLab},
		},
		{
			name:          "code-server pod is terminating and already deleted machinelearning when request type is code-server",
			image:         existImageName,
			machine:       testutils.TestMachineName,
			wantError:     true,
			kubeResources: []client.Object{availableMachine, cephfs, terminatingCodeServer},
		},
		{
			name:          "jupyterlab pod is terminating and already deleted machinelearning when request type is code-server",
			image:         existImageName,
			machine:       testutils.TestMachineName,
			wantError:     true,
			kubeResources: []client.Object{availableMachine, cephfs, terminatingJupyterLab},
		},
		{
			name:          "code-server pod is running and already deleted machinelearning when request type is code-server",
			image:         existImageName,
			machine:       testutils.TestMachineName,
			wantError:     true,
			kubeResources: []client.Object{availableMachine, cephfs, codeServer},
		},
		{
			name:          "jupyterlab pod is running and already deleted machinelearning when request type is code-server",
			image:         existImageName,
			machine:       testutils.TestMachineName,
			wantError:     true,
			kubeResources: []client.Object{availableMachine, cephfs, jupyterLab},
		},
		{
			name:          "machinelearning have already exist when request type is code-server",
			image:         existImageName,
			machine:       testutils.TestMachineName,
			wantError:     true,
			kubeResources: []client.Object{availableMachine, cephfs, machineLearning},
		},
		{
			name:          "enter not existing machine name when request type is code-server",
			image:         existImageName,
			machine:       "notExistMachine",
			wantError:     true,
			kubeResources: []client.Object{availableMachine, cephfs, machineLearning},
		},
	}

	for _, test := range deployCodeServerTestCases {
		t.Run(test.name, func(t *testing.T) {
			c, err := testutils.NewFakeKubeClient(test.kubeResources)
			if err != nil {
				t.Error(err)
			}
			command := NewCreateAppCmd(c, os.Stdout)
			flags := map[string]string{
				"images":   test.image,
				"machines": test.machine,
				"type":     cksoperatorv1beta2.CodeServer.String(),
			}
			err = testutils.StartFakeCommandWithOptions(flags, make([]string, 0), os.Stdout, command)

			if (err != nil) != test.wantError {
				t.Errorf("expected error: %v, got '%v'\n", test.wantError, err)
			}
		})
	}

	deployJupyterLabTestCases := []struct {
		name          string
		image         string
		machine       string
		wantError     bool
		kubeResources []client.Object
	}{
		{
			name:          "create codeserver applications successfully when request type is jupyterlab",
			image:         existImageName,
			machine:       testutils.TestMachineName,
			wantError:     false,
			kubeResources: []client.Object{availableMachine, cephfs},
		},
		{
			name:          "image was not found when request type is jupyterlab",
			image:         notExistImageName,
			machine:       testutils.TestMachineName,
			wantError:     true,
			kubeResources: []client.Object{availableMachine},
		},
		{
			name:          "there are not available machine when request type is jupyterlab",
			image:         existImageName,
			machine:       testutils.TestMachineName,
			wantError:     true,
			kubeResources: []client.Object{unavailableMachine, cephfs},
		},
		{
			name:          "there are not available machine when request type is jupyterlab by app alias subcommand",
			image:         existImageName,
			machine:       testutils.TestMachineName,
			wantError:     true,
			kubeResources: []client.Object{unavailableMachine, cephfs},
		},
		{
			name:          "cephFS not found when request type is jupyterlab",
			image:         existImageName,
			machine:       testutils.TestMachineName,
			wantError:     true,
			kubeResources: []client.Object{availableMachine},
		},
		{
			name:          "running code-server pod and exist machinelearning when request type is jupyterlab",
			image:         existImageName,
			machine:       testutils.TestMachineName,
			wantError:     true,
			kubeResources: []client.Object{availableMachine, cephfs, machineLearning, codeServer},
		},
		{
			name:          "running jupyterlab pod and exist machinelearning when request type is jupyterlab",
			image:         existImageName,
			machine:       testutils.TestMachineName,
			wantError:     true,
			kubeResources: []client.Object{availableMachine, cephfs, machineLearning, jupyterLab},
		},
		{
			name:          "code-server pod is terminating and already deleted machinelearning when request type is jupyterlab",
			image:         existImageName,
			machine:       testutils.TestMachineName,
			wantError:     true,
			kubeResources: []client.Object{availableMachine, cephfs, terminatingCodeServer},
		},
		{
			name:          "jupyterlab pod is terminating and already deleted machinelearning when request type is jupyterlab",
			image:         existImageName,
			machine:       testutils.TestMachineName,
			wantError:     true,
			kubeResources: []client.Object{availableMachine, cephfs, terminatingJupyterLab},
		},
		{
			name:          "code-server pod is running and already deleted machinelearning when request type is jupyterlab",
			image:         existImageName,
			machine:       testutils.TestMachineName,
			wantError:     true,
			kubeResources: []client.Object{availableMachine, cephfs, codeServer},
		},
		{
			name:          "jupyterlab pod is running and already deleted machinelearning when request type is jupyterlab",
			image:         existImageName,
			machine:       testutils.TestMachineName,
			wantError:     true,
			kubeResources: []client.Object{availableMachine, cephfs, jupyterLab},
		},
		{
			name:          "machinelearning have already exist when request type is jupyterlab",
			image:         existImageName,
			machine:       testutils.TestMachineName,
			wantError:     true,
			kubeResources: []client.Object{availableMachine, cephfs, machineLearning},
		},
		{
			name:          "enter not existing machine name when request type is jupyterlab",
			image:         existImageName,
			machine:       "notExistMachine",
			wantError:     true,
			kubeResources: []client.Object{availableMachine, cephfs, machineLearning},
		},
	}

	for _, test := range deployJupyterLabTestCases {
		t.Run(test.name, func(t *testing.T) {
			c, err := testutils.NewFakeKubeClient(test.kubeResources)
			if err != nil {
				t.Error(err)
			}
			command := NewCreateAppCmd(c, os.Stdout)
			flags := map[string]string{
				"images":   test.image,
				"machines": test.machine,
				"type":     cksoperatorv1beta2.JupyterLab.String(),
			}
			err = testutils.StartFakeCommandWithOptions(flags, make([]string, 0), os.Stdout, command)

			if (err != nil) != test.wantError {
				t.Errorf("expected error: %v, got '%v'\n", test.wantError, err)
			}
		})
	}
}

func TestCreateApplicationsImageNameAutoComplete(t *testing.T) {

	testCases := []struct {
		name          string
		input         string
		expected      []string
		kubeResources []client.Object
	}{
		{
			name:  "display all image names when c was input as images option",
			input: "c",
			expected: []string{
				"cuda11.5.1-cudnn8",
				"cuda11.5.0-cudnn8",
				"cuda11.4.3-cudnn8",
				"cuda11.4.2-cudnn8",
				"cuda11.4.1-cudnn8",
				"cuda11.4.0-cudnn8",
				"cuda11.3.1-cudnn8",
				"cuda11.3.0-cudnn8",
				"cuda11.2.2-cudnn8",
				"cuda11.2.1-cudnn8",
				"cuda11.2.0-cudnn8",
				"cuda11.1.1-cudnn8",
				"cuda11.0.3-cudnn8",
				"cuda10.2-cudnn8",
				"cuda10.2-cudnn7",
			},
		},
		{
			name: "display cuda11.2.x when cuda11.2 was input as images option",
			expected: []string{
				"cuda11.2.2-cudnn8",
				"cuda11.2.1-cudnn8",
				"cuda11.2.0-cudnn8",
			},
			input: "cuda11.2",
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx := context.Background()
			gc, err := image.NewGitHubClient(ctx)
			if err != nil {
				t.Error(err)
			}
			actual, _ := compImageName(ctx, test.input, gc)
			if diff := cmp.Diff(actual, test.expected, consts.CmpSliceOpts...); diff != "" {
				t.Errorf("Diff: \n%v", diff)
			}
		})
	}
}

func TestCreateApplicationsMachineNameAutoComplete(t *testing.T) {

	const (
		allResourcesAvailable  = "all-resources-available"
		halfResourcesAvailable = "half-resources-available"
		allResourcesInUse      = "all-resources-in-use"
	)

	machine := imperatorv1alpha1.Machine{
		TypeMeta: metav1.TypeMeta{
			APIVersion: imperatorv1alpha1.GroupVersion.String(),
			Kind:       "Machine",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: consts.MachineResourceName,
		},
		Spec: imperatorv1alpha1.MachineSpec{
			MachineTypes: []imperatorv1alpha1.MachineType{
				{
					Name: allResourcesAvailable,
					Spec: imperatorv1alpha1.MachineDetailSpec{
						CPU:    resource.MustParse("2"),
						Memory: resource.MustParse("64Gi"),
						GPU: &imperatorv1alpha1.GPUSpec{
							Type:   "nvidia.com/gpu",
							Family: "ampere",
							Num:    resource.MustParse("2"),
						},
					},
					Available: 2,
				},
				{
					Name: halfResourcesAvailable,
					Spec: imperatorv1alpha1.MachineDetailSpec{
						CPU:    resource.MustParse("4"),
						Memory: resource.MustParse("32Gi"),
					},
					Available: 4,
				},
				{
					Name: allResourcesInUse,
					Spec: imperatorv1alpha1.MachineDetailSpec{
						CPU:    resource.MustParse("12"),
						Memory: resource.MustParse("128Gi"),
						GPU: &imperatorv1alpha1.GPUSpec{
							Type:   "nvidia.com/gpu",
							Family: "pascal",
							Num:    resource.MustParse("2"),
						},
					},
					Available: 6,
				},
			},
		},
		Status: imperatorv1alpha1.MachineStatus{
			AvailableMachines: []imperatorv1alpha1.AvailableMachineCondition{
				{
					Name: allResourcesAvailable,
					Usage: imperatorv1alpha1.UsageCondition{
						Maximum:  2,
						Reserved: 2,
						Used:     0,
						Waiting:  0,
					},
				},
				{
					Name: halfResourcesAvailable,
					Usage: imperatorv1alpha1.UsageCondition{
						Maximum:  4,
						Reserved: 2,
						Used:     2,
						Waiting:  0,
					},
				},
				{
					Name: allResourcesInUse,
					Usage: imperatorv1alpha1.UsageCondition{
						Maximum:  6,
						Reserved: 0,
						Used:     4,
						Waiting:  2,
					},
				},
			},
		},
	}

	testCases := []struct {
		name          string
		input         string
		expected      []string
		kubeResources []client.Object
	}{
		{
			name:          "display all-resources-available when a was input as machines option",
			input:         "a",
			expected:      []string{"all-resources-available"},
			kubeResources: []client.Object{&machine},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			c, err := testutils.NewFakeKubeClient(test.kubeResources)
			if err != nil {
				t.Error(err)
			}
			actual, _ := compMachineName(test.input, c)
			if diff := cmp.Diff(actual, test.expected, consts.CmpSliceOpts...); diff != "" {
				t.Errorf("Diff: \n%v", diff)
			}
		})
	}
}
