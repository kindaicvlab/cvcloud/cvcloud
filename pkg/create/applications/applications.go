/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package applications

import (
	"context"
	"fmt"
	"io"
	"log"
	"strings"

	"github.com/spf13/cobra"
	imperatorv1alpha1 "github.com/tenzen-y/imperator/pkg/api/v1alpha1"
	cksoperatorv1beta2 "gitlab.com/kindaicvlab/cvcloud/cks-operator/api/v1beta2"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/consts"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/image"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/kubectl"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/utils"
)

type createAppOptions struct {
	images   string
	machines string
	appType  string
}

const createAppDescription = `
The command create applications to run and edit codes.
`

func NewCreateAppCmd(client *kubectl.Client, stdOut io.Writer) *cobra.Command {

	var (
		o   = &createAppOptions{}
		ctx = context.Background()
	)

	// create GitHub client
	gc, err := image.NewGitHubClient(ctx)
	if err != nil {
		log.Fatal(err)
	}

	cmd := &cobra.Command{
		Use:          "applications",
		Aliases:      consts.ApplicationsAlias,
		Short:        "create applications to run and edit codes",
		Long:         createAppDescription,
		SilenceUsage: true,
		RunE: func(cmd *cobra.Command, args []string) error {

			imageExistFlag := false
			mlImageNameList, err := gc.CreateMLImageNameList(ctx)
			if err != nil {
				return err
			}

			for _, imageName := range mlImageNameList {
				if imageName != o.images {
					continue
				}
				imageExistFlag = true
			}

			if !imageExistFlag {
				return fmt.Errorf("images '%s', was not found\n", o.images)
			}

			if err := o.checkMachineInUse(client); err != nil {
				return err
			}

			return o.run(client, stdOut)
		},
	}

	f := cmd.Flags()

	f.StringVarP(&o.images, "images", "i", "cuda11.1.1-cudnn8", "set container images")
	if err := cmd.RegisterFlagCompletionFunc("images", func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		return compImageName(ctx, toComplete, gc)
	}); err != nil {
		log.Fatal(err)
	}

	f.StringVarP(&o.machines, "machines", "m", "spot-machine", "set machines name")
	if err := cmd.RegisterFlagCompletionFunc("machines", func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		return compMachineName(toComplete, client)
	}); err != nil {
		log.Fatal(err)
	}

	f.StringVarP(&o.appType, "type", "t", "codeserver", "set applications type, jupyterlab or codeserver")
	if err := cmd.RegisterFlagCompletionFunc("type", func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		return []string{cksoperatorv1beta2.JupyterLab.String(), cksoperatorv1beta2.CodeServer.String()}, cobra.ShellCompDirectiveDefault
	}); err != nil {
		log.Fatal(err)
	}

	return cmd

}

func (o *createAppOptions) run(client *kubectl.Client, stdOut io.Writer) error {

	var (
		machineLearnings cksoperatorv1beta2.MachineLearningList
		codeserverPods   corev1.PodList
		jupyterlabPods   corev1.PodList
		cephFSPVC        corev1.PersistentVolumeClaim
	)

	if err := client.ListResources(&machineLearnings, map[string]string{}); err != nil {
		return err
	}
	if err := client.ListCodeServerPods(&codeserverPods); err != nil {
		return err
	}
	if err := client.ListJupyterLabPods(&jupyterlabPods); err != nil {
		return err
	}
	if err := client.GetResources(&cephFSPVC, strings.Join([]string{client.GetNamespace(), "pvc"}, "-")); err != nil {
		return fmt.Errorf("your persistent volume was not found: firstly, you must create persistent volume, err: %v", err)
	}

	switch {
	case len(machineLearnings.Items) > 0:
		return fmt.Errorf("your applications already have created")
	case len(codeserverPods.Items) > 0:
		if err := checkTerminatingPods(codeserverPods); err != nil {
			return err
		}
		return fmt.Errorf("your applications already have created")
	case len(jupyterlabPods.Items) > 0:
		if err := checkTerminatingPods(jupyterlabPods); err != nil {
			return err
		}
		return fmt.Errorf("your applications already have created")
	}

	requestedApp := cksoperatorv1beta2.MachineLearning{
		TypeMeta: metav1.TypeMeta{
			APIVersion: cksoperatorv1beta2.GroupVersion.String(),
			Kind:       "MachineLearning",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: strings.Join([]string{
				"machinelearning",
				client.GetNamespace(),
			}, "-"),
			Namespace: client.GetNamespace(),
		},
		Spec: cksoperatorv1beta2.MachineLearningSpec{
			MachineLearningApps: []cksoperatorv1beta2.MachineLearningApps{
				{
					AppName:   strings.Join([]string{o.appType, client.GetNamespace()}, "-"),
					Namespace: client.GetNamespace(),
					MLAppSpec: cksoperatorv1beta2.MLAppSpec{
						Type: cksoperatorv1beta2.ApplicationType(o.appType),
						Machine: cksoperatorv1beta2.MachineSpec{
							Group: consts.MachineResourceName,
							Type:  o.machines,
						},
						AppImage: utils.GetRegistryNameTag(o.images),
					},
				},
			},
		},
	}

	if err := client.CreateResources(&requestedApp); err != nil {
		return err
	}
	if _, err := fmt.Fprintln(stdOut, "Request Successfully!"); err != nil {
		return err
	}

	return nil

}

func (o *createAppOptions) checkMachineInUse(client *kubectl.Client) error {

	machine := &imperatorv1alpha1.Machine{}
	if err := client.GetMachines(machine, consts.MachineResourceName); err != nil {
		return err
	}
	var targetMachineCondition imperatorv1alpha1.UsageCondition
	for _, mc := range machine.Status.AvailableMachines {
		if mc.Name != o.machines {
			continue
		}
		targetMachineCondition = mc.Usage
	}

	if targetMachineCondition.Reserved == 0 {
		return fmt.Errorf("machine: '%s' is unavailable", o.machines)
	}

	return nil
}

func checkTerminatingPods(targetPods corev1.PodList) error {
	for _, pod := range targetPods.Items {
		if pod.GetObjectMeta().GetDeletionTimestamp() != nil {
			return fmt.Errorf("your application is terminating")
		}
	}
	return nil
}

func compImageName(ctx context.Context, toComplete string, gc *image.GitHub) ([]string, cobra.ShellCompDirective) {

	var targetImageNameList []string
	availableImageList, err := gc.CreateMLImageNameList(ctx)
	if err != nil {
		fmt.Println(err)
		return targetImageNameList, cobra.ShellCompDirectiveError
	}

	for _, imName := range availableImageList {
		if strings.HasPrefix(imName, toComplete) {
			targetImageNameList = append(targetImageNameList, imName)
		}
	}

	return targetImageNameList, cobra.ShellCompDirectiveDefault
}

func compMachineName(toComplete string, client *kubectl.Client) ([]string, cobra.ShellCompDirective) {

	machine := &imperatorv1alpha1.Machine{}
	var machineNameList []string
	if err := client.GetMachines(machine, consts.MachineResourceName); err != nil {
		return machineNameList, cobra.ShellCompDirectiveError
	}

	for _, machineCondition := range machine.Status.AvailableMachines {
		if strings.HasPrefix(machineCondition.Name, toComplete) && machineCondition.Usage.Reserved > 0 {
			machineNameList = append(machineNameList, machineCondition.Name)
		}
	}
	return machineNameList, cobra.ShellCompDirectiveDefault
}
