/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package consts

import (
	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
)

const (
	DefaultVolumeSize                 int16 = 300
	MaxUserStorageSize                int16 = 2000
	MachineResourceName                     = "general-machines"
	NfsProvisionerName                      = "cks-managed-nfs-storage"
	FullBackup                              = "fullbackup"
	MlImageDocsLink                         = "https://github.com/KindaiCVLAB/machinelearning-images/blob/master/versions"
	VersionDirectoryNameRegexpPattern       = `cuda[.0-9].*-cudnn[0-9]`
	PipRequirementsFileName                 = "requirements.txt"
	MLImageRegistryName                     = "ghcr.io/kindaicvlab/machinelearning-images"
)

var (
	ApplicationsAlias = []string{"application", "app"}
	StoragesAlias     = []string{"storage", "st"}
	MachinesAlias     = []string{"machine", "ma"}
	ImagesAlias       = []string{"image", "im"}
	CmpSliceOpts      = []cmp.Option{
		cmpopts.SortSlices(func(i, j int) bool {
			return i < j
		}),
	}
)
