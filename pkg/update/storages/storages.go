/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package storages

import (
	"fmt"
	"io"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/consts"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/kubectl"
)

type updateStorageOptions struct {
	size int16
}

const updateStorageDescription = `
The command updates capacity size of your created storages.
`

func NewUpdateStorageCmd(client *kubectl.Client, stdOut io.Writer) *cobra.Command {

	o := &updateStorageOptions{}

	cmd := &cobra.Command{
		Use:          "storages",
		Aliases:      consts.StoragesAlias,
		Short:        "update capacity size of your created storages",
		Long:         updateStorageDescription,
		SilenceUsage: true,
		RunE: func(cmd *cobra.Command, args []string) error {
			return o.run(client, stdOut)
		},
	}

	f := cmd.Flags()
	f.Int16VarP(&o.size, "size", "s", consts.DefaultVolumeSize+100, "require persistent volume size")

	return cmd
}

func (o *updateStorageOptions) run(client *kubectl.Client, stdOut io.Writer) error {

	if o.size > consts.MaxUserStorageSize {
		return fmt.Errorf("Forbidden user is available '%dGiB' or less", int(consts.MaxUserStorageSize))
	}

	var (
		cephFS corev1.PersistentVolumeClaim
	)
	if err := client.GetCephFSPVC(&cephFS); err != nil {
		return err
	}

	rawExistingStorageSize := cephFS.Spec.Resources.Requests.Storage().String()
	existingStorageUnitIndex := strings.Index(rawExistingStorageSize, "Gi")
	existingStorageCapacity := rawExistingStorageSize[:existingStorageUnitIndex]

	existingStorageSize, err := strconv.Atoi(existingStorageCapacity)
	if err != nil {
		return err
	}

	if existingStorageSize >= int(o.size) {
		return fmt.Errorf("your request '%d' is invalid: request size must be greater than existing storage capacity, '%sGiB'", int(o.size), existingStorageCapacity)
	}

	updateSize := strings.Join([]string{strconv.Itoa(int(o.size)), "Gi"}, "")
	cephFS.Spec.Resources = corev1.ResourceRequirements{
		Requests: corev1.ResourceList{
			corev1.ResourceStorage: resource.MustParse(updateSize),
		},
	}

	if err := client.UpdateResources(&cephFS); err != nil {
		return err
	}

	if _, err := fmt.Fprintln(stdOut, "Request Successfully"); err != nil {
		return err
	}
	return nil

}
