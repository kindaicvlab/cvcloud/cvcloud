/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package storages

import (
	"os"
	"strings"
	"testing"

	cksoperatorconsts "gitlab.com/kindaicvlab/cvcloud/cks-operator/consts"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/testutils"
)

func TestUpdateStorages(t *testing.T) {

	const (
		disallowedStorageSize    = "2001"
		lessThanExistingStorages = "10"
		equalExistingStorages    = testutils.TestStorageSize
		testUpdateStoragesSize   = "200"
	)

	rookCephFSCSI := "rook-cephfs"
	cephFS := &corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name:      strings.Join([]string{testutils.TestNameSpace, "pvc"}, "-"),
			Namespace: testutils.TestNameSpace,
			Labels: map[string]string{
				cksoperatorconsts.CksStorageTypeLabelKey: cksoperatorconsts.CephFsLabel,
			},
		},
		Spec: corev1.PersistentVolumeClaimSpec{
			AccessModes: []corev1.PersistentVolumeAccessMode{
				corev1.ReadWriteMany,
			},
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					corev1.ResourceStorage: resource.MustParse(strings.Join([]string{
						testutils.TestStorageSize,
						"Gi",
					}, "")),
				},
			},
			StorageClassName: &rookCephFSCSI,
		},
		Status: corev1.PersistentVolumeClaimStatus{
			Phase: corev1.ClaimBound,
		},
	}

	testCases := []struct {
		name          string
		size          string
		wantError     bool
		kubeResources []client.Object
	}{
		{
			name:          "update successfully",
			size:          testUpdateStoragesSize,
			wantError:     false,
			kubeResources: []client.Object{cephFS},
		},
		{
			name:          "requested storages size is disallowed",
			size:          disallowedStorageSize,
			wantError:     true,
			kubeResources: []client.Object{cephFS},
		},
		{
			name:          "requested storages size is less than existing storages",
			size:          lessThanExistingStorages,
			wantError:     true,
			kubeResources: []client.Object{cephFS},
		},
		{
			name:          "requested storages size is equal existing storages",
			size:          equalExistingStorages,
			wantError:     true,
			kubeResources: []client.Object{cephFS},
		},
		{
			name:      "There is not storages resource",
			size:      testUpdateStoragesSize,
			wantError: true,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			c, err := testutils.NewFakeKubeClient(test.kubeResources)
			if err != nil {
				t.Error(err)
			}
			command := NewUpdateStorageCmd(c, os.Stdout)
			flags := map[string]string{
				"size": test.size,
			}
			err = testutils.StartFakeCommandWithOptions(flags, make([]string, 0), os.Stdout, command)

			if (err != nil) != test.wantError {
				t.Errorf("expected error: %v, got '%v'\n", test.wantError, err)
			}
		})
	}

}
