/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package update

import (
	"io"

	"github.com/spf13/cobra"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/kubectl"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/update/storages"
)

const updateDescription = `
This command consists of multiple subcommand which can be used to
update the resources, including

- The capacity size of your created storages
`

func NewUpdateCmd(stdOut io.Writer) *cobra.Command {
	cmd := &cobra.Command{
		Use:          "update",
		Short:        "update capacity size of your created storages",
		Long:         updateDescription,
		SilenceUsage: true,
	}

	client := &kubectl.Client{}
	cmd.AddCommand(storages.NewUpdateStorageCmd(client, stdOut))

	return cmd
}
