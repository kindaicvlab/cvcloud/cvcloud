/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package kubectl

import (
	"log"

	imperatorv1alpha1 "github.com/tenzen-y/imperator/pkg/api/v1alpha1"
	cksoperatorv1beta2 "gitlab.com/kindaicvlab/cvcloud/cks-operator/api/v1beta2"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/tools/clientcmd"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/config"
)

// Client is client to use kubernetes API
type Client struct {
	KubeClient    client.Client
	UserNameSpace string
}

// getKubeClient returns client for kubernetes and namespace for user
func (c *Client) getKubeClient() error {

	if c.KubeClient != nil && c.UserNameSpace != "" {
		return nil
	}

	// get namespace
	var err error
	if c.UserNameSpace == "" {
		c.UserNameSpace = c.GetNamespace()
	}

	// generate Client for kubernetes
	if c.KubeClient == nil {
		scm := runtime.NewScheme()
		if err := scheme.AddToScheme(scm); err != nil {
			return err
		}
		if err := cksoperatorv1beta2.AddToScheme(scm); err != nil {
			return err
		}
		if err := imperatorv1alpha1.AddToScheme(scm); err != nil {
			return err
		}
		cfg := config.GetConfigOrDie()
		cfg.QPS = 100
		cfg.Burst = 100

		c.KubeClient, err = client.New(cfg, client.Options{Scheme: scm})
		if err != nil {
			return err
		}
	}

	return nil
}

func (c *Client) GetNamespace() string {
	if c.UserNameSpace != "" {
		return c.UserNameSpace
	}
	loadingRules := clientcmd.NewDefaultClientConfigLoadingRules()
	configOverrides := &clientcmd.ConfigOverrides{}
	kubeConfig := clientcmd.NewNonInteractiveDeferredLoadingClientConfig(loadingRules, configOverrides)
	ns, _, err := kubeConfig.Namespace()
	if err != nil {
		log.Fatal(err)
	}
	return ns
}
