/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package kubectl

import (
	"strings"
	"time"

	cksoperatorconsts "gitlab.com/kindaicvlab/cvcloud/cks-operator/consts"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	"k8s.io/utils/pointer"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/consts"
)

//CreateCephFS creates pvc for rook-ceph
func (c *Client) CreateCephFS(cephFS *corev1.PersistentVolumeClaim, cephVolume string) error {

	if err := c.getKubeClient(); err != nil {
		return err
	}

	cephFS.SetName(strings.Join([]string{c.UserNameSpace, "pvc"}, "-"))
	cephFS.SetNamespace(c.UserNameSpace)

	label := map[string]string{
		cksoperatorconsts.CksStorageTypeLabelKey: cksoperatorconsts.CephFsLabel,
	}
	if cephFS.ObjectMeta.Labels == nil {
		cephFS.ObjectMeta.Labels = make(map[string]string)
	}
	cephFS.ObjectMeta.Labels = label

	cephFS.Spec = corev1.PersistentVolumeClaimSpec{
		AccessModes: []corev1.PersistentVolumeAccessMode{
			corev1.ReadWriteMany,
		},
		Resources: corev1.ResourceRequirements{
			Requests: corev1.ResourceList{
				corev1.ResourceStorage: resource.MustParse(cephVolume),
			},
		},
		StorageClassName: pointer.String("rook-cephfs"),
	}

	if err := c.CreateResources(cephFS); err != nil {
		return err
	}

	return nil
}

//CreateNFS creates pvc for cks-managed-nfs-provider
func (c *Client) CreateNFS(nfs *corev1.PersistentVolumeClaim, nfsVolume string) error {

	var (
		t      = time.Now()
		layout = "20210105-1504"
	)

	nfs.SetName(strings.Join([]string{c.UserNameSpace, "nfs", t.Format(layout)}, "-"))
	nfs.SetNamespace(c.UserNameSpace)

	if nfs.ObjectMeta.Annotations == nil {
		nfs.ObjectMeta.Annotations = make(map[string]string)
	}
	nfs.ObjectMeta.Annotations = map[string]string{
		corev1.BetaStorageClassAnnotation: consts.NfsProvisionerName,
	}

	label := map[string]string{
		cksoperatorconsts.CksStorageTypeLabelKey: cksoperatorconsts.NfsLabel,
	}
	if nfs.ObjectMeta.Labels == nil {
		nfs.ObjectMeta.Labels = make(map[string]string)
	}
	nfs.ObjectMeta.Labels = label

	nfs.Spec = corev1.PersistentVolumeClaimSpec{
		AccessModes: []corev1.PersistentVolumeAccessMode{
			corev1.ReadWriteMany,
		},
		Resources: corev1.ResourceRequirements{
			Requests: corev1.ResourceList{
				corev1.ResourceStorage: resource.MustParse(nfsVolume),
			},
		},
	}

	if err := c.CreateResources(nfs); err != nil {
		return err
	}

	return nil
}
