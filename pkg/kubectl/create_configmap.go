/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package kubectl

import (
	"strings"

	cksoperatorconsts "gitlab.com/kindaicvlab/cvcloud/cks-operator/consts"
	corev1 "k8s.io/api/core/v1"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/consts"
)

//CreateConfigMap create configmap for full backup
func (c *Client) CreateConfigMap(config *corev1.ConfigMap) error {

	if err := c.getKubeClient(); err != nil {
		return err
	}

	config.SetName(cksoperatorconsts.BackUpConfigMapName)
	config.SetNamespace(c.UserNameSpace)

	// set labels
	labels := map[string]string{
		cksoperatorconsts.CksAppTypeLabelKey: consts.FullBackup,
	}

	// set metadata.labels
	if config.ObjectMeta.Labels == nil {
		config.ObjectMeta.Labels = make(map[string]string)
	}
	config.ObjectMeta.Labels = labels

	// determine env vars
	envVars := map[string]string{
		"original-dir":  strings.Join([]string{c.UserNameSpace, "pvc/"}, "-"),
		"dest-base-dir": "/nas/backup/full/",
		"dest-diff-dir": "/nas/backup/diff/",
		"keep-dirs-num": "5",
	}

	// set data
	if config.Data == nil {
		config.Data = make(map[string]string)
	}
	config.Data = envVars

	if err := c.CreateResources(config); err != nil {
		return err
	}

	return nil
}
