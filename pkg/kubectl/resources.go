/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package kubectl

import (
	"context"
	"fmt"
	"sort"
	"strconv"
	"strings"
	"sync"

	imperatorv1alpha1 "github.com/tenzen-y/imperator/pkg/api/v1alpha1"
	cksoperatorv1beta2 "gitlab.com/kindaicvlab/cvcloud/cks-operator/api/v1beta2"
	cksoperatorconsts "gitlab.com/kindaicvlab/cvcloud/cks-operator/consts"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/labels"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

//ListResources will return resources list
func (c *Client) ListResources(resources client.ObjectList, label map[string]string) error {
	if err := c.getKubeClient(); err != nil {
		return err
	}
	ctx := context.Background()
	if err := c.KubeClient.List(ctx, resources, &client.ListOptions{
		Namespace:     c.UserNameSpace,
		LabelSelector: labels.SelectorFromSet(label),
	}); err != nil {
		return err
	}
	return nil
}

//ListCodeServerPods will return pods list launching code-server
func (c *Client) ListCodeServerPods(pods *corev1.PodList) error {
	if err := c.ListResources(pods, map[string]string{
		cksoperatorconsts.CksAppTypeLabelKey: cksoperatorv1beta2.CodeServer.String(),
	}); err != nil {
		return err
	}
	return nil
}

//ListJupyterLabPods will return pods list launching JupyterLab Server
func (c *Client) ListJupyterLabPods(pods *corev1.PodList) error {
	if err := c.ListResources(pods, map[string]string{
		cksoperatorconsts.CksAppTypeLabelKey: cksoperatorv1beta2.JupyterLab.String(),
	}); err != nil {
		return err
	}
	return nil
}

//GetResources get resources
func (c *Client) GetResources(resources client.Object, name string) error {
	if err := c.getKubeClient(); err != nil {
		return err
	}
	ctx := context.Background()
	if err := c.KubeClient.Get(ctx, client.ObjectKey{
		Namespace: c.UserNameSpace,
		Name:      name,
	}, resources); err != nil {
		return err
	}
	return nil
}

//GetMachines get machines resources
func (c *Client) GetMachines(machines *imperatorv1alpha1.Machine, name string) error {
	if err := c.getKubeClient(); err != nil {
		return err
	}
	ctx := context.Background()
	if err := c.KubeClient.Get(ctx, client.ObjectKey{
		Name: name,
	}, machines); err != nil {
		return err
	}
	return nil
}

//GetCephFSPVC get CephFS PVC
func (c *Client) GetCephFSPVC(pvc *corev1.PersistentVolumeClaim) error {
	if err := c.GetResources(pvc, strings.Join([]string{c.GetNamespace(), "pvc"}, "-")); err != nil {
		return err
	}
	return nil
}

//CreateResources create resources
func (c *Client) CreateResources(resources client.Object) error {
	if err := c.getKubeClient(); err != nil {
		return err
	}
	ctx := context.Background()
	if err := c.KubeClient.Create(ctx, resources, &client.CreateOptions{}); err != nil {
		return err
	}
	return nil
}

//DeleteResources delete resources
func (c *Client) DeleteResources(resources client.Object) error {
	if err := c.getKubeClient(); err != nil {
		return err
	}
	ctx := context.Background()
	if err := c.KubeClient.Delete(ctx, resources, &client.DeleteOptions{}); err != nil {
		return err
	}
	return nil
}

//UpdateResources update resources
func (c *Client) UpdateResources(resources client.Object) error {
	if err := c.getKubeClient(); err != nil {
		return err
	}
	ctx := context.Background()
	if err := c.KubeClient.Update(ctx, resources, &client.UpdateOptions{}); err != nil {
		return err
	}
	return nil
}

//NFSPVCDetailInfo is detail information for NFS pvc
type NFSPVCDetailInfo struct {
	Name        string
	StatusPhase string
	CreatedYear int
	CreatedDay  int
	CreatedTime int
}

//GetLatestPVCByNFSProvisioner searches pvc created latest
func (c *Client) GetLatestPVCByNFSProvisioner() (*NFSPVCDetailInfo, error) {

	pvcList := &corev1.PersistentVolumeClaimList{}
	if err := c.ListResources(pvcList, map[string]string{
		cksoperatorconsts.CksStorageTypeLabelKey: cksoperatorconsts.NfsLabel,
	}); err != nil {
		return nil, err
	}
	if len(pvcList.Items) == 0 {
		return nil, fmt.Errorf("your persistent volume provided by cks managed NFS storage is not found")
	}

	var (
		wg            sync.WaitGroup
		pvcDetailInfo []NFSPVCDetailInfo
	)
	for _, partOfPVC := range pvcList.Items {
		wg.Add(1)

		go func(p corev1.PersistentVolumeClaim) {
			defer wg.Done()

			pvcStatusPhase := string(p.Status.Phase)
			pvcNameSlice := strings.Split(p.Name, "-")
			pvcCreatedYear, _ := strconv.Atoi(pvcNameSlice[2][0:3])
			pvcCreatedDay, _ := strconv.Atoi(pvcNameSlice[2][4:7])
			pvcCreatedTime, _ := strconv.Atoi(pvcNameSlice[3])

			pvcDetailInfo = append(pvcDetailInfo, NFSPVCDetailInfo{
				StatusPhase: pvcStatusPhase,
				Name:        p.Name,
				CreatedYear: pvcCreatedYear,
				CreatedDay:  pvcCreatedDay,
				CreatedTime: pvcCreatedTime,
			})
		}(partOfPVC)
	}
	wg.Wait()
	sort.SliceStable(pvcDetailInfo, func(i, j int) bool { return pvcDetailInfo[i].CreatedYear > pvcDetailInfo[j].CreatedYear })
	sort.SliceStable(pvcDetailInfo, func(i, j int) bool { return pvcDetailInfo[i].CreatedDay > pvcDetailInfo[j].CreatedDay })
	sort.SliceStable(pvcDetailInfo, func(i, j int) bool { return pvcDetailInfo[i].CreatedTime > pvcDetailInfo[j].CreatedTime })

	latestNfsPvc := &pvcDetailInfo[0]
	if latestNfsPvc.Name == "" {
		return nil, fmt.Errorf("nfs pvc name is not found")
	}
	return latestNfsPvc, nil
}
