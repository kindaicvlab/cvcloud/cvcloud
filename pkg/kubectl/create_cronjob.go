/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package kubectl

import (
	"strings"
	"time"

	cksoperatorconsts "gitlab.com/kindaicvlab/cvcloud/cks-operator/consts"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	"k8s.io/utils/pointer"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/consts"
)

//CreateCronJob create cronjob manifest for backup
func (c *Client) CreateCronJob(cronJob *batchv1.CronJob) error {

	if err := c.getKubeClient(); err != nil {
		return err
	}

	namespace := c.UserNameSpace
	cronJob.SetName(strings.Join([]string{namespace, "pvc", consts.FullBackup}, "-"))
	cronJob.SetNamespace(namespace)

	// set labels
	label := map[string]string{
		cksoperatorconsts.CksAppTypeLabelKey: consts.FullBackup,
	}

	// set metadata.labels
	if cronJob.ObjectMeta.Labels == nil {
		cronJob.ObjectMeta.Labels = make(map[string]string)
	}
	cronJob.ObjectMeta.Labels = label

	cronJob.Spec = batchv1.CronJobSpec{
		Schedule:                   "51 23 * * *",
		ConcurrencyPolicy:          batchv1.ForbidConcurrent,
		StartingDeadlineSeconds:    pointer.Int64(300),
		SuccessfulJobsHistoryLimit: pointer.Int32(5),
		FailedJobsHistoryLimit:     pointer.Int32(5),
		Suspend:                    pointer.Bool(false),
	}

	cronJob.Spec.JobTemplate.Spec = batchv1.JobSpec{
		Completions:  pointer.Int32(1),
		Parallelism:  pointer.Int32(1),
		BackoffLimit: pointer.Int32(1),
	}

	// set job labels
	if cronJob.Spec.JobTemplate.Spec.Template.ObjectMeta.Labels == nil {
		cronJob.Spec.JobTemplate.Spec.Template.ObjectMeta.Labels = make(map[string]string)
	}
	cronJob.Spec.JobTemplate.Spec.Template.ObjectMeta.Labels = label

	// set affinity
	cronJob.Spec.JobTemplate.Spec.Template.Spec.Affinity = &corev1.Affinity{
		NodeAffinity: &corev1.NodeAffinity{
			PreferredDuringSchedulingIgnoredDuringExecution: []corev1.PreferredSchedulingTerm{
				{
					Weight: 100,
					Preference: corev1.NodeSelectorTerm{
						MatchExpressions: []corev1.NodeSelectorRequirement{
							{
								Key:      "hardware-type",
								Operator: corev1.NodeSelectorOpIn,
								Values: []string{
									"NonGPU",
								},
							},
						},
					},
				},
			},
		},
	}

	// set security context
	if cronJob.Spec.JobTemplate.Spec.Template.Spec.SecurityContext == nil {
		cronJob.Spec.JobTemplate.Spec.Template.Spec.SecurityContext = &corev1.PodSecurityContext{
			RunAsNonRoot: pointer.Bool(true),
			RunAsUser:    pointer.Int64(1000),
		}
	}

	// check pvc status
	var (
		latestNfsPvc *NFSPVCDetailInfo
		err          error
	)
	start := time.Now()
	for time.Since(start).Seconds() < ProcessTimeout {
		latestNfsPvc, err = c.GetLatestPVCByNFSProvisioner()
		if err == nil {
			break
		}
	}
	if err != nil {
		return err
	}

	initCommand := strings.Join([]string{"mkdir -p ${DEST_BASE_DIR}",
		"mkdir -p ${DEST_DIFF_DIR}",
		"DIRS_NUM=$(ls ${DEST_BASE_DIR} | wc -w)",
		"RM_DIRS=$(ls -d ${DEST_BASE_DIR}* | xargs readlink -f | head -n $((${DIRS_NUM}-${KEEP_DIRS_NUM})))",
		"if [ ${DIRS_NUM} -gt ${KEEP_DIRS_NUM} ];then rm -vr ${RM_DIRS};fi"}, "\n")

	backUpCommand := strings.Join([]string{"DEST_FULL_FILE=$(date \"+%Y%m%d-%H%M%S\")",
		"rsync -rlOtcv --delete ${ORIGINAL_DIR} ${DEST_BASE_DIR}${DEST_FULL_FILE}"}, "\n")

	containerResourceRequirements := corev1.ResourceRequirements{
		Limits: corev1.ResourceList{
			corev1.ResourceCPU:    resource.MustParse("2000m"),
			corev1.ResourceMemory: resource.MustParse("16Gi"),
		},
		Requests: corev1.ResourceList{
			corev1.ResourceCPU:    resource.MustParse("1000m"),
			corev1.ResourceMemory: resource.MustParse("8Gi"),
		},
	}

	cronJob.Spec.JobTemplate.Spec.Template.Spec.InitContainers = []corev1.Container{
		{
			Name:  "rm-container",
			Image: backUpImage,
			Env: []corev1.EnvVar{
				{
					Name: "DEST_BASE_DIR",
					ValueFrom: &corev1.EnvVarSource{
						ConfigMapKeyRef: &corev1.ConfigMapKeySelector{
							Key: "dest-base-dir",
							LocalObjectReference: corev1.LocalObjectReference{
								Name: cksoperatorconsts.BackUpConfigMapName,
							},
						},
					},
				},
				{
					Name: "DEST_DIFF_DIR",
					ValueFrom: &corev1.EnvVarSource{
						ConfigMapKeyRef: &corev1.ConfigMapKeySelector{
							Key: "dest-diff-dir",
							LocalObjectReference: corev1.LocalObjectReference{
								Name: cksoperatorconsts.BackUpConfigMapName,
							},
						},
					},
				},
				{
					Name: "KEEP_DIRS_NUM",
					ValueFrom: &corev1.EnvVarSource{
						ConfigMapKeyRef: &corev1.ConfigMapKeySelector{
							Key: "keep-dirs-num",
							LocalObjectReference: corev1.LocalObjectReference{
								Name: cksoperatorconsts.BackUpConfigMapName,
							},
						},
					},
				},
			},
			Command: []string{
				"sh",
				"-c",
				initCommand,
			},
			VolumeMounts: []corev1.VolumeMount{
				{
					Name:      strings.Join([]string{namespace, "pvc"}, "-"),
					MountPath: strings.Join([]string{"/", namespace, "-pvc"}, ""),
				},
				{
					Name:      latestNfsPvc.Name,
					MountPath: "/nas",
				},
			},
			Resources: containerResourceRequirements,
		},
	}

	cronJob.Spec.JobTemplate.Spec.Template.Spec.Containers = []corev1.Container{
		{
			Name:  "fullbackup-container",
			Image: backUpImage,
			Env: []corev1.EnvVar{
				{
					Name: "ORIGINAL_DIR",
					ValueFrom: &corev1.EnvVarSource{
						ConfigMapKeyRef: &corev1.ConfigMapKeySelector{
							Key: "original-dir",
							LocalObjectReference: corev1.LocalObjectReference{
								Name: cksoperatorconsts.BackUpConfigMapName,
							},
						},
					},
				},
				{
					Name: "DEST_BASE_DIR",
					ValueFrom: &corev1.EnvVarSource{
						ConfigMapKeyRef: &corev1.ConfigMapKeySelector{
							Key: "dest-base-dir",
							LocalObjectReference: corev1.LocalObjectReference{
								Name: cksoperatorconsts.BackUpConfigMapName,
							},
						},
					},
				},
			},
			Command: []string{
				"sh",
				"-c",
				backUpCommand,
			},
			VolumeMounts: []corev1.VolumeMount{
				{
					Name:      strings.Join([]string{namespace, "pvc"}, "-"),
					MountPath: strings.Join([]string{"/", namespace, "-pvc"}, ""),
				},
				{
					Name:      latestNfsPvc.Name,
					MountPath: "/nas",
				},
			},
			Resources: containerResourceRequirements,
		},
	}

	cronJob.Spec.JobTemplate.Spec.Template.Spec.Volumes = []corev1.Volume{
		{
			Name: strings.Join([]string{namespace, "pvc"}, "-"),
			VolumeSource: corev1.VolumeSource{
				PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
					ClaimName: strings.Join([]string{namespace, "pvc"}, "-"),
					ReadOnly:  false,
				},
			},
		},
		{
			Name: latestNfsPvc.Name,
			VolumeSource: corev1.VolumeSource{
				PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
					ClaimName: latestNfsPvc.Name,
					ReadOnly:  false,
				},
			},
		},
	}

	cronJob.Spec.JobTemplate.Spec.Template.Spec.RestartPolicy = corev1.RestartPolicyNever

	if err := c.CreateResources(cronJob); err != nil {
		return err
	}

	return nil
}
