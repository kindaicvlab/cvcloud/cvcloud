/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package initialize

import (
	"gopkg.in/yaml.v3"
)

// KubeConfig is kubeconfig
type KubeConfig struct {
	APIVersion     string     `yaml:"apiVersion"`
	Clusters       []Clusters `yaml:"clusters"`
	Contexts       []Contexts `yaml:"contexts"`
	CurrentContext string     `yaml:"current-context"`
	Kind           string     `yaml:"kind"`
	Users          []Users    `yaml:"users"`
}

// Clusters is .clusters
type Clusters struct {
	Cluster     Cluster `yaml:"cluster"`
	ClusterName string  `yaml:"name"`
}

// Cluster is .clusters.cluster
type Cluster struct {
	Ca     string `yaml:"certificate-authority-data"`
	Server string `yaml:"server"`
}

// Contexts is .contexts
type Contexts struct {
	Context     KubeContext `yaml:"context"`
	ContextName string      `yaml:"name"`
}

// KubeContext is .contexts.context
type KubeContext struct {
	ContextsCluster  string `yaml:"cluster"`
	ContextNamespace string `yaml:"namespace"`
	ContextUser      string `yaml:"user"`
}

// Users is .users
type Users struct {
	UserName string `yaml:"name"`
	User     User   `yaml:"user"`
}

// User is .users.user
type User struct {
	UserToken string `yaml:"token"`
}

func (k *KubeConfig) checkKubeConfigSchema(configData *[]byte) error {
	if err := yaml.Unmarshal(*configData, &k); err != nil {
		return err
	}
	return nil
}
