/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package initialize

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/utils"
)

type initOptions struct {
	userName          string
	githubAccessToken string
}

const initDescription = `
The command initialize cvcloud.
`

func NewInitCmd(stdOut io.Writer) *cobra.Command {

	o := &initOptions{}

	cmd := &cobra.Command{
		Use:          "init",
		Short:        "initialize cvcloud",
		Long:         initDescription,
		SilenceUsage: true,
		ValidArgs:    []string{""},
		Args:         cobra.ExactValidArgs(0),
		RunE: func(cmd *cobra.Command, args []string) error {
			if o.userName == "" {
				return fmt.Errorf("specify your name in behind --uname option")
			}
			return o.run(stdOut)
		},
	}

	f := cmd.Flags()
	f.StringVarP(&o.userName, "uname", "u", "", "set your name")
	f.StringVarP(&o.githubAccessToken, "token", "t", "", "set your github access token")

	return cmd
}

var (
	//InitAccessEndpoint is endpoint to download credential data
	InitAccessEndpoint = "http://cvcloud.cks.local/kubeconfig"
)

func (o *initOptions) run(stdOut io.Writer) error {

	url := strings.Join([]string{InitAccessEndpoint, o.userName}, "/")
	res, err := http.Get(url)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		return fmt.Errorf("uname '%s' was not found", o.userName)
	}

	kubeConfigData, err := io.ReadAll(res.Body)
	if err != nil {
		return err
	}

	kubeConfigSchema := &KubeConfig{}
	if err := kubeConfigSchema.checkKubeConfigSchema(&kubeConfigData); err != nil {
		return fmt.Errorf("can not parse kubeConfig schema, error: %v", err)
	}

	homeDir, err := os.UserHomeDir()
	if err != nil {
		return err
	}
	kubeDir := filepath.Join(homeDir, ".kube")
	if !utils.FileExists(kubeDir) {
		if err = os.Mkdir(kubeDir, 0760); err != nil {
			return err
		}
	}

	kubeConfigPath := filepath.Join(kubeDir, "cvcloud")
	if err := os.WriteFile(kubeConfigPath, kubeConfigData, 0600); err != nil {
		return err
	}
	kubeConfigFileData, err := os.ReadFile(kubeConfigPath)
	if err != nil {
		return err
	}

	if err := kubeConfigSchema.checkKubeConfigSchema(&kubeConfigFileData); err != nil {
		return err
	}

	if o.githubAccessToken != "" {
		if err := o.setupGitHubAccessToken(stdOut); err != nil {
			return err
		}
	}

	if _, err := fmt.Fprintln(stdOut, "initialization complete"); err != nil {
		return err
	}
	return nil
}

func (o *initOptions) setupGitHubAccessToken(stdout io.Writer) error {
	tokenFilePath, err := utils.GetTokenFilePath()
	if err != nil {
		return err
	}
	if err = os.WriteFile(tokenFilePath, []byte(o.githubAccessToken), 0600); err != nil {
		return err
	}
	if _, err := fmt.Fprintf(stdout, "write github personal access token to %s\n", tokenFilePath); err != nil {
		return err
	}
	return nil
}
