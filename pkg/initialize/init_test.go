/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package initialize

import (
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/testutils"
)

func TestInit(t *testing.T) {
	testCases := []struct {
		name      string
		flags     map[string]string
		args      []string
		wantError bool
	}{
		{
			name:      "wrong args case",
			args:      []string{"foo"},
			wantError: true,
		},
		{
			name:      "wrong option case",
			flags:     map[string]string{"bar": "true"},
			wantError: true,
		},
		{
			name:      "empty uname",
			flags:     map[string]string{"uname": ""},
			wantError: true,
		},
		{
			name:      "good-user access test server",
			flags:     map[string]string{"uname": "good-user"},
			wantError: false,
		},
		{
			name:      "not-exist user access test server",
			flags:     map[string]string{"uname": "not-exist"},
			wantError: true,
		},
		{
			name:      "bad-user access test server",
			flags:     map[string]string{"uname": "bad-user"},
			wantError: true,
		},
		{
			name:      "create github personal access token",
			flags:     map[string]string{"uname": "good-user", "token": "foo"},
			wantError: false,
		},
	}

	originHOME := os.Getenv("HOME")
	if err := os.Setenv("HOME", "/tmp"); err != nil {
		log.Fatal(err)
	}

	testServer := httptest.NewServer(http.FileServer(http.Dir("testdata")))
	defer testServer.Close()

	for _, test := range testCases {
		for i := 0; i < 2; i++ {
			t.Run(test.name, func(t *testing.T) {
				InitAccessEndpoint = testServer.URL
				command := NewInitCmd(os.Stdout)
				err := testutils.StartFakeCommandWithOptions(test.flags, test.args, os.Stdout, command)
				if (err != nil) != test.wantError {
					t.Errorf("expected error: %v, got '%v'\n", test.wantError, err)
				}
			})
		}
	}

	if err := os.RemoveAll("/tmp/.kube"); err != nil {
		log.Fatal(err)
	}
	if err := os.Setenv("HOME", originHOME); err != nil {
		log.Fatal(err)
	}
}
