/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package storages

import (
	"bytes"
	"path"
	"strings"
	"testing"
	"time"

	cksoperatorconsts "gitlab.com/kindaicvlab/cvcloud/cks-operator/consts"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/utils/pointer"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/consts"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/testutils"
)

func TestGetStorages(t *testing.T) {

	cephfs := &corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name:      strings.Join([]string{testutils.TestNameSpace, "pvc"}, "-"),
			Namespace: testutils.TestNameSpace,
			Labels: map[string]string{
				cksoperatorconsts.CksStorageTypeLabelKey: cksoperatorconsts.CephFsLabel,
			},
		},
		Spec: corev1.PersistentVolumeClaimSpec{
			AccessModes: []corev1.PersistentVolumeAccessMode{
				corev1.ReadWriteMany,
			},
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					corev1.ResourceStorage: resource.MustParse(strings.Join([]string{
						testutils.TestStorageSize,
						"Gi",
					}, "")),
				},
			},
			StorageClassName: pointer.String("rook-cephfs"),
		},
		Status: corev1.PersistentVolumeClaimStatus{
			Phase: corev1.ClaimBound,
		},
	}

	var (
		now               = time.Now()
		OldPVCCreatedTime = now.AddDate(0, 0, -1)
		layout            = "20210105-1504"
		pvMode            = corev1.PersistentVolumeFilesystem
	)

	usingNFSPVC := &corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name:      strings.Join([]string{testutils.TestNameSpace, cksoperatorconsts.NfsLabel, now.Format(layout)}, "-"),
			Namespace: testutils.TestNameSpace,
			Labels: map[string]string{
				cksoperatorconsts.CksStorageTypeLabelKey: cksoperatorconsts.NfsLabel,
			},
			Annotations: map[string]string{
				corev1.BetaStorageClassAnnotation: consts.NfsProvisionerName,
			},
		},
		Spec: corev1.PersistentVolumeClaimSpec{
			AccessModes: []corev1.PersistentVolumeAccessMode{
				corev1.ReadWriteMany,
			},
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					corev1.ResourceStorage: resource.MustParse(strings.Join([]string{
						testutils.TestStorageSize,
						"Gi",
					}, "")),
				},
			},
		},
		Status: corev1.PersistentVolumeClaimStatus{
			Phase: corev1.ClaimBound,
		},
	}

	usingNFSPV := &corev1.PersistentVolume{
		ObjectMeta: metav1.ObjectMeta{
			Name:      strings.Join([]string{testutils.TestNameSpace, "nfs", now.Format(layout)}, "-"),
			Namespace: testutils.TestNameSpace,
			Annotations: map[string]string{
				"pv.kubernetes.io/provisioned-by": "provisioner.cks/nfs",
			},
		},
		Spec: corev1.PersistentVolumeSpec{
			AccessModes: []corev1.PersistentVolumeAccessMode{
				corev1.ReadWriteMany,
			},
			Capacity: corev1.ResourceList{
				corev1.ResourceStorage: resource.MustParse(strings.Join([]string{
					testutils.TestStorageSize,
					"Gi",
				}, "")),
			},
			ClaimRef: &corev1.ObjectReference{
				APIVersion: corev1.SchemeGroupVersion.String(),
				Kind:       "PersistentVolumeClaim",
				Name:       strings.Join([]string{testutils.TestNameSpace, "nfs", now.Format(layout)}, "-"),
				Namespace:  testutils.TestNameSpace,
			},
			MountOptions: []string{
				"nfsvers=4",
			},
			PersistentVolumeSource: corev1.PersistentVolumeSource{
				NFS: &corev1.NFSVolumeSource{
					Path: path.Join("/share/cks-managed-nfs-storage",
						strings.Join([]string{
							testutils.TestNameSpace,
							testutils.TestNameSpace,
							"nfs",
							"using",
						}, "-")),
					Server: "xxx.xxx.xxx.xxx",
				},
			},
			PersistentVolumeReclaimPolicy: corev1.PersistentVolumeReclaimPolicy("Retain"),
			StorageClassName:              consts.NfsProvisionerName,
			VolumeMode:                    &pvMode,
		},
		Status: corev1.PersistentVolumeStatus{
			Phase: corev1.VolumeBound,
		},
	}

	oldNFSPV := &corev1.PersistentVolume{
		ObjectMeta: metav1.ObjectMeta{
			Name:      strings.Join([]string{testutils.TestNameSpace, "nfs", OldPVCCreatedTime.Format(layout)}, "-"),
			Namespace: testutils.TestNameSpace,
			Annotations: map[string]string{
				"pv.kubernetes.io/provisioned-by": "provisioner.cks/nfs",
			},
		},
		Spec: corev1.PersistentVolumeSpec{
			AccessModes: []corev1.PersistentVolumeAccessMode{
				corev1.ReadWriteMany,
			},
			Capacity: corev1.ResourceList{
				corev1.ResourceStorage: resource.MustParse(strings.Join([]string{
					testutils.TestStorageSize,
					"Gi",
				}, "")),
			},
			ClaimRef: &corev1.ObjectReference{
				APIVersion: corev1.SchemeGroupVersion.String(),
				Kind:       "PersistentVolumeClaim",
				Name:       strings.Join([]string{testutils.TestNameSpace, "pvc", "old"}, "-"),
				Namespace:  testutils.TestNameSpace,
			},
			MountOptions: []string{
				"nfsvers=4",
			},
			PersistentVolumeSource: corev1.PersistentVolumeSource{
				NFS: &corev1.NFSVolumeSource{
					Path: path.Join("/share/cks-managed-nfs-storage",
						strings.Join([]string{
							testutils.TestNameSpace,
							testutils.TestNameSpace,
							"nfs",
							"old",
						}, "-")),
					Server: "xxx.xxx.xxx.xxx",
				},
			},
			PersistentVolumeReclaimPolicy: corev1.PersistentVolumeReclaimPolicy("Retain"),
			StorageClassName:              consts.NfsProvisionerName,
			VolumeMode:                    &pvMode,
		},
		Status: corev1.PersistentVolumeStatus{
			Phase: corev1.VolumeReleased,
		},
	}

	const otherUserName = "iwai.tz"
	otherUserNFSPV := &corev1.PersistentVolume{
		ObjectMeta: metav1.ObjectMeta{
			Name:      strings.Join([]string{otherUserName, "nfs", now.Format(layout)}, "-"),
			Namespace: otherUserName,
			Annotations: map[string]string{
				"pv.kubernetes.io/provisioned-by": "provisioner.cks/nfs",
			},
		},
		Spec: corev1.PersistentVolumeSpec{
			AccessModes: []corev1.PersistentVolumeAccessMode{
				corev1.ReadWriteMany,
			},
			Capacity: corev1.ResourceList{
				corev1.ResourceStorage: resource.MustParse(strings.Join([]string{
					testutils.TestStorageSize,
					"Gi",
				}, "")),
			},
			ClaimRef: &corev1.ObjectReference{
				APIVersion: corev1.SchemeGroupVersion.String(),
				Kind:       "PersistentVolumeClaim",
				Name:       strings.Join([]string{otherUserName, "nfs", now.Format(layout)}, "-"),
				Namespace:  otherUserName,
			},
			MountOptions: []string{
				"nfsvers=4",
			},
			PersistentVolumeSource: corev1.PersistentVolumeSource{
				NFS: &corev1.NFSVolumeSource{
					Path: path.Join("/share/cks-managed-nfs-storage",
						strings.Join([]string{
							otherUserName,
							otherUserName,
							"nfs",
							"using",
						}, "-")),
					Server: "xxx.xxx.xxx.xxx",
				},
			},
			PersistentVolumeReclaimPolicy: corev1.PersistentVolumeReclaimPolicy("Retain"),
			StorageClassName:              consts.NfsProvisionerName,
			VolumeMode:                    &pvMode,
		},
		Status: corev1.PersistentVolumeStatus{
			Phase: corev1.VolumeBound,
		},
	}

	otherUserNameSpace := corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: otherUserName,
		},
	}

	const getStoragesGoldenOutput = `CAPACITY	STATUS	NAS DIR                                                        
100GiB  	Bound 	/share/share/cks-managed-nfs-storage/yuki.tz-yuki.tz-nfs-using	
`

	testCases := []struct {
		name          string
		kubeResources []client.Object
		golden        string
		wantError     bool
	}{
		{
			name:          "get storages successfully",
			golden:        getStoragesGoldenOutput,
			wantError:     false,
			kubeResources: []client.Object{&otherUserNameSpace, cephfs, usingNFSPVC, usingNFSPV, oldNFSPV, otherUserNFSPV},
		},
		{
			name:          "get storage successfully when using storage command",
			golden:        getStoragesGoldenOutput,
			wantError:     false,
			kubeResources: []client.Object{&otherUserNameSpace, cephfs, usingNFSPVC, usingNFSPV, oldNFSPV, otherUserNFSPV},
		},
		{
			name:          "nfs storage could not found",
			wantError:     true,
			kubeResources: []client.Object{cephfs},
		},
		{
			name:      "storages was not found",
			wantError: true,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			c, err := testutils.NewFakeKubeClient(test.kubeResources)
			if err != nil {
				t.Error(err)
			}
			buf := &bytes.Buffer{}
			command := NewGetStorageCmd(c, buf)
			err = testutils.StartFakeCommandWithOptions(make(map[string]string), make([]string, 0), buf, command)

			if (err != nil) != test.wantError {
				t.Errorf("expected error: %v, got '%v'\n", test.wantError, err)
			} else if !strings.EqualFold(buf.String(), test.golden) {
				t.Errorf("does not match golden,\nWANT:\n%s\nGOT:\n%s\n", test.golden, buf.String())
			}
		})
	}

}
