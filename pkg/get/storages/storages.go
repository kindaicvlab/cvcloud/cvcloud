/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package storages

import (
	"fmt"
	"io"
	"path"
	"strings"

	"github.com/spf13/cobra"
	cksoperatorconsts "gitlab.com/kindaicvlab/cvcloud/cks-operator/consts"
	corev1 "k8s.io/api/core/v1"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/consts"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/kubectl"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/utils"
)

type getStorageOptions struct{}

const getStorageDescription = `
The command gets your created storages.
`

func NewGetStorageCmd(client *kubectl.Client, stdOut io.Writer) *cobra.Command {

	o := &getStorageOptions{}

	cmd := &cobra.Command{
		Use:          "storages",
		Aliases:      consts.StoragesAlias,
		Short:        "get your created storages",
		Long:         getStorageDescription,
		SilenceUsage: true,
		RunE: func(cmd *cobra.Command, args []string) error {
			return o.run(client, stdOut)
		},
	}

	return cmd
}

func (o *getStorageOptions) run(client *kubectl.Client, stdOut io.Writer) error {

	var (
		pvc        corev1.PersistentVolumeClaim
		nfsPVCs    corev1.PersistentVolumeClaimList
		nfsPVs     corev1.PersistentVolumeList
		usingNFSPV corev1.PersistentVolume
	)

	if err := client.GetResources(&pvc, strings.Join([]string{client.GetNamespace(), "pvc"}, "-")); err != nil {
		return fmt.Errorf("your persistent volume could not found, error: %v", err)
	}

	//List nfs storage
	if err := client.ListResources(&nfsPVCs, map[string]string{
		cksoperatorconsts.CksStorageTypeLabelKey: cksoperatorconsts.NfsLabel,
	}); err != nil {
		return err
	} else if len(nfsPVCs.Items) == 0 {
		return fmt.Errorf("your nfs volume could not found")
	}

	if err := client.ListResources(&nfsPVs, map[string]string{}); err != nil {
		return err
	}

	for _, pv := range nfsPVs.Items {

		if pv.Spec.ClaimRef.Namespace != client.GetNamespace() ||
			pv.Status.Phase != corev1.VolumeBound ||
			pv.Spec.ClaimRef.Name != nfsPVCs.Items[0].Name ||
			pv.Spec.StorageClassName != consts.NfsProvisionerName {
			continue
		}

		usingNFSPV = pv
		break
	}

	data := [][]string{
		{
			strings.Join([]string{pvc.Spec.Resources.Requests.Storage().String(), "B"}, ""),
			string(pvc.Status.Phase),
			path.Join("/share/share/cks-managed-nfs-storage", path.Base(usingNFSPV.Spec.NFS.Path)),
		},
	}
	header := []string{"Capacity", "Status", "Nas Dir"}
	utils.CreateTable(header, data, stdOut)
	return nil
}
