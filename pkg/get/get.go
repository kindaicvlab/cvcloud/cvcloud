/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package get

import (
	"io"

	"github.com/spf13/cobra"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/get/applications"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/get/images"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/get/machines"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/get/storages"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/kubectl"
)

const getDescription = `
This command consists of multiple subcommands which can be used to
get extended information about the resources provided cvcloud, including:

- The available machines types
- The available container images
- The running applications
- The created storages
`

func NewGetCmd(stdOut io.Writer) *cobra.Command {

	cmd := &cobra.Command{
		Use:          "get",
		Short:        "get machines, applications, storages or available container images",
		Long:         getDescription,
		SilenceUsage: true,
	}

	client := &kubectl.Client{}
	cmd.AddCommand(applications.NewGetAppCmd(client, stdOut))
	cmd.AddCommand(storages.NewGetStorageCmd(client, stdOut))
	cmd.AddCommand(images.NewGetImageCmd(stdOut))
	cmd.AddCommand(machines.NewGetMachinesCmd(client, stdOut))

	return cmd

}
