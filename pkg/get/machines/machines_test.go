/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package machines

import (
	"bytes"
	"k8s.io/apimachinery/pkg/api/resource"
	"strings"
	"testing"

	imperatorv1alpha1 "github.com/tenzen-y/imperator/pkg/api/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/consts"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/testutils"
)

func TestGetMachines(t *testing.T) {

	const (
		allResourcesAvailable         = "all-resources-available"
		halfResourcesAvailable        = "half-resources-available"
		halfResourcesAvailableWithGpu = "half-resources-available-with-gpu"
		allResourcesInUse             = "all-resources-in-use"
	)

	machine := imperatorv1alpha1.Machine{
		TypeMeta: metav1.TypeMeta{
			APIVersion: imperatorv1alpha1.GroupVersion.String(),
			Kind:       "Machine",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: consts.MachineResourceName,
		},
		Spec: imperatorv1alpha1.MachineSpec{
			MachineTypes: []imperatorv1alpha1.MachineType{
				{
					Name: allResourcesAvailable,
					Spec: imperatorv1alpha1.MachineDetailSpec{
						CPU:    resource.MustParse("2"),
						Memory: resource.MustParse("64Gi"),
						GPU: &imperatorv1alpha1.GPUSpec{
							Type:    "nvidia.com/gpu",
							Num:     resource.MustParse("3"),
							Product: "NVIDIA-GeForce-RTX-3090",
						},
					},
					Available: 2,
				},
				{
					Name: halfResourcesAvailable,
					Spec: imperatorv1alpha1.MachineDetailSpec{
						CPU:    resource.MustParse("4"),
						Memory: resource.MustParse("32Gi"),
					},
					Available: 4,
				},
				{
					Name: halfResourcesAvailableWithGpu,
					Spec: imperatorv1alpha1.MachineDetailSpec{
						CPU:    resource.MustParse("4"),
						Memory: resource.MustParse("32Gi"),
						GPU: &imperatorv1alpha1.GPUSpec{
							Type:   "nvidia.com/gpu",
							Num:    resource.MustParse("2"),
							Family: "ampere",
						},
					},
					Available: 4,
				},
				{
					Name: allResourcesInUse,
					Spec: imperatorv1alpha1.MachineDetailSpec{
						CPU:    resource.MustParse("12"),
						Memory: resource.MustParse("128Gi"),
						GPU: &imperatorv1alpha1.GPUSpec{
							Type:    "nvidia.com/gpu",
							Num:     resource.MustParse("2"),
							Machine: "DGX-A100",
						},
					},
					Available: 6,
				},
			},
		},
		Status: imperatorv1alpha1.MachineStatus{
			AvailableMachines: []imperatorv1alpha1.AvailableMachineCondition{
				{
					Name: allResourcesAvailable,
					Usage: imperatorv1alpha1.UsageCondition{
						Maximum:  2,
						Reserved: 2,
						Used:     0,
						Waiting:  0,
					},
				},
				{
					Name: halfResourcesAvailable,
					Usage: imperatorv1alpha1.UsageCondition{
						Maximum:  4,
						Reserved: 2,
						Used:     2,
						Waiting:  0,
					},
				},
				{
					Name: halfResourcesAvailableWithGpu,
					Usage: imperatorv1alpha1.UsageCondition{
						Maximum:  4,
						Reserved: 2,
						Used:     2,
						Waiting:  0,
					},
				},
				{
					Name: allResourcesInUse,
					Usage: imperatorv1alpha1.UsageCondition{
						Maximum:  6,
						Reserved: 0,
						Used:     5,
						Waiting:  1,
					},
				},
			},
		},
	}

	testCases := []struct {
		name          string
		golden        string
		flags         map[string]string
		kubeResources []client.Object
	}{
		{
			name: "display available machines resource",
			golden: `MACHINE NAME                     	CPU	SYSTEM MEMORY	GPU TYPE               	NUMBER OF GPUS	MACHINE AVAILABLE 
all-resources-available          	2  	64GiB        	NVIDIA-GeForce-RTX-3090	3             	2                	
half-resources-available         	4  	32GiB        	none                   	0             	2                	
half-resources-available-with-gpu	4  	32GiB        	ampere                 	2             	2                	
`,
			kubeResources: []client.Object{&machine},
		},
		{
			name:  "display all machines resource",
			flags: map[string]string{"all": "true"},
			golden: `MACHINE NAME                     	CPU	SYSTEM MEMORY	GPU TYPE               	NUMBER OF GPUS	MACHINE AVAILABLE 
all-resources-available          	2  	64GiB        	NVIDIA-GeForce-RTX-3090	3             	2                	
half-resources-available         	4  	32GiB        	none                   	0             	2                	
half-resources-available-with-gpu	4  	32GiB        	ampere                 	2             	2                	
all-resources-in-use             	12 	128GiB       	DGX-A100               	2             	0                	
`,
			kubeResources: []client.Object{&machine},
		},
		{
			name:  "display all machines resource sorted by cpu when using option --all --cpu",
			flags: map[string]string{"all": "true", "cpu": "true"},
			golden: `MACHINE NAME                     	CPU	SYSTEM MEMORY	GPU TYPE               	NUMBER OF GPUS	MACHINE AVAILABLE 
all-resources-in-use             	12 	128GiB       	DGX-A100               	2             	0                	
half-resources-available         	4  	32GiB        	none                   	0             	2                	
half-resources-available-with-gpu	4  	32GiB        	ampere                 	2             	2                	
all-resources-available          	2  	64GiB        	NVIDIA-GeForce-RTX-3090	3             	2                	
`,
			kubeResources: []client.Object{&machine},
		},
		{
			name:  "display all machines resource sorted by memory when using option --all --memory",
			flags: map[string]string{"all": "true", "memory": "true"},
			golden: `MACHINE NAME                     	CPU	SYSTEM MEMORY	GPU TYPE               	NUMBER OF GPUS	MACHINE AVAILABLE 
all-resources-in-use             	12 	128GiB       	DGX-A100               	2             	0                	
all-resources-available          	2  	64GiB        	NVIDIA-GeForce-RTX-3090	3             	2                	
half-resources-available         	4  	32GiB        	none                   	0             	2                	
half-resources-available-with-gpu	4  	32GiB        	ampere                 	2             	2                	
`,
			kubeResources: []client.Object{&machine},
		},
		{
			name:  "display all machines resource sorted by gpu when using option --all --gpu",
			flags: map[string]string{"all": "true", "gpu": "true"},
			golden: `MACHINE NAME                     	CPU	SYSTEM MEMORY	GPU TYPE               	NUMBER OF GPUS	MACHINE AVAILABLE 
all-resources-available          	2  	64GiB        	NVIDIA-GeForce-RTX-3090	3             	2                	
half-resources-available-with-gpu	4  	32GiB        	ampere                 	2             	2                	
all-resources-in-use             	12 	128GiB       	DGX-A100               	2             	0                	
half-resources-available         	4  	32GiB        	none                   	0             	2                	
`,
			kubeResources: []client.Object{&machine},
		},
		{
			name:  "display all machines resource sorted by available when using option --all --available",
			flags: map[string]string{"all": "true", "available": "true"},
			golden: `MACHINE NAME                     	CPU	SYSTEM MEMORY	GPU TYPE               	NUMBER OF GPUS	MACHINE AVAILABLE 
all-resources-available          	2  	64GiB        	NVIDIA-GeForce-RTX-3090	3             	2                	
half-resources-available         	4  	32GiB        	none                   	0             	2                	
half-resources-available-with-gpu	4  	32GiB        	ampere                 	2             	2                	
all-resources-in-use             	12 	128GiB       	DGX-A100               	2             	0                	
`,
			kubeResources: []client.Object{&machine},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			c, err := testutils.NewFakeKubeClient(test.kubeResources)
			if err != nil {
				t.Fatal(err)
			}
			buf := &bytes.Buffer{}
			command := NewGetMachinesCmd(c, buf)
			if err := testutils.StartFakeCommandWithOptions(test.flags, make([]string, 0), buf, command); err != nil {
				t.Error(err)
			}

			if !strings.EqualFold(buf.String(), test.golden) {
				t.Errorf("does not match golden,\nWANT:\n%s\nGOT:\n%s\n", test.golden, buf.String())
			}
		})
	}
}
