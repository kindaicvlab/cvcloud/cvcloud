/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package machines

import (
	"github.com/spf13/cobra"
	imperatorv1alpha1 "github.com/tenzen-y/imperator/pkg/api/v1alpha1"
	"io"
	"sort"
	"strconv"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/consts"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/kubectl"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/utils"
)

type getMachinesOptions struct {
	showAll   bool
	cpu       bool
	memory    bool
	gpu       bool
	available bool
}

const getMachinesDescription = `
This command gets available machines types.
`

func NewGetMachinesCmd(client *kubectl.Client, stdOut io.Writer) *cobra.Command {

	o := &getMachinesOptions{}

	cmd := &cobra.Command{
		Use:          "machines",
		Aliases:      consts.MachinesAlias,
		Short:        "get available machines types",
		Long:         getMachinesDescription,
		SilenceUsage: true,
		RunE: func(cmd *cobra.Command, args []string) error {
			return o.run(client, stdOut)
		},
	}

	f := cmd.Flags()
	f.BoolVarP(&o.showAll, "all", "A", false, "get all machines")
	f.BoolVarP(&o.cpu, "cpu", "C", false, "sort machine by cpu")
	f.BoolVarP(&o.memory, "memory", "M", false, "sort machine by mempry")
	f.BoolVarP(&o.gpu, "gpu", "G", false, "sort machine by gpu")
	f.BoolVarP(&o.available, "available", "L", false, "sort machine by available machine resources")

	return cmd
}

func (o *getMachinesOptions) run(client *kubectl.Client, stdOut io.Writer) error {

	var (
		machine       imperatorv1alpha1.Machine
		data          [][]string
		machineNames  []string
		mtSpecNames   = make(map[string]imperatorv1alpha1.MachineDetailSpec)
		mtStatusNames = make(map[string]imperatorv1alpha1.UsageCondition)
	)

	if err := client.GetMachines(&machine, consts.MachineResourceName); err != nil {
		return err
	}

	for _, mtSpec := range machine.Spec.MachineTypes {
		mtSpecNames[mtSpec.Name] = mtSpec.Spec
		machineNames = append(machineNames, mtSpec.Name)
	}
	for _, mtStatus := range machine.Status.AvailableMachines {
		mtStatusNames[mtStatus.Name] = mtStatus.Usage
	}

	if o.cpu || o.memory || o.gpu || o.available {
		o.sortMachine(machineNames, mtSpecNames, mtStatusNames)
	}

	for _, name := range machineNames {

		available := mtStatusNames[name].Reserved
		spec := mtSpecNames[name]
		gpuNum := "0"
		gpuType := "none"

		if !o.showAll && available == 0 {
			continue
		}

		if spec.GPU != nil {
			gpuNum = spec.GPU.Num.String()
			if spec.GPU.Machine != "" {
				gpuType = spec.GPU.Machine
			} else if spec.GPU.Family != "" {
				gpuType = spec.GPU.Family
			} else if spec.GPU.Product != "" {
				gpuType = spec.GPU.Product
			}
		}

		elements := []string{
			name,
			spec.CPU.String(),
			spec.Memory.String() + "B",
			gpuType,
			gpuNum,
			strconv.Itoa(int(available)),
		}
		data = append(data, elements)

	}

	header := []string{"Machine name", "CPU", "System Memory", "GPU Type", "Number Of GPUs", "Machine Available"}
	utils.CreateTable(header, data, stdOut)

	return nil

}

func (o *getMachinesOptions) sortMachine(machineNames []string, machineDetail map[string]imperatorv1alpha1.MachineDetailSpec, machineUsage map[string]imperatorv1alpha1.UsageCondition) {

	switch {
	case o.cpu:
		sort.SliceStable(machineNames, func(i, j int) bool {
			machineI := machineDetail[machineNames[i]]
			machineJ := machineDetail[machineNames[j]]
			return machineI.CPU.Value() > machineJ.CPU.Value()
		})
	case o.memory:
		sort.SliceStable(machineNames, func(i, j int) bool {
			machineI := machineDetail[machineNames[i]]
			machineJ := machineDetail[machineNames[j]]
			return machineI.Memory.Value() > machineJ.Memory.Value()
		})
	case o.gpu:
		sort.SliceStable(machineNames, func(i, j int) bool {
			machineI := machineDetail[machineNames[i]]
			machineJ := machineDetail[machineNames[j]]
			var (
				machineIGPU int64 = 0
				machineJGPU int64 = 0
			)
			if machineI.GPU != nil {
				machineIGPU = machineI.GPU.Num.Value()
			}
			if machineJ.GPU != nil {
				machineJGPU = machineJ.GPU.Num.Value()
			}
			return machineIGPU > machineJGPU
		})
	case o.available:
		sort.SliceStable(machineNames, func(i, j int) bool {
			return machineUsage[machineNames[i]].Reserved > machineUsage[machineNames[j]].Reserved
		})
	}
}
