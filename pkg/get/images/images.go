/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package images

import (
	"context"
	"io"
	"strings"

	"github.com/spf13/cobra"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/consts"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/image"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/utils"
)

const (
	getImageDescription = `
The command gets available container images.
`
)

type getImagesOptions struct {
	showDetail bool
}

func NewGetImageCmd(stdOut io.Writer) *cobra.Command {

	o := &getImagesOptions{}

	cmd := &cobra.Command{
		Use:          "images",
		Aliases:      consts.ImagesAlias,
		Short:        "get available container images",
		Long:         getImageDescription,
		SilenceUsage: true,
		RunE: func(cmd *cobra.Command, args []string) error {
			return o.run(stdOut)
		},
	}

	f := cmd.Flags()
	f.BoolVarP(&o.showDetail, "detail", "D", false, "show image detail")

	return cmd
}

func (o *getImagesOptions) run(stdOut io.Writer) error {

	var (
		header []string
		data   [][]string
		ctx    = context.Background()
	)

	gc, err := image.NewGitHubClient(ctx)
	if err != nil {
		return err
	}
	mlImageNames, err := gc.CreateMLImageNameList(ctx)
	if err != nil {
		return err
	}

	if !o.showDetail {

		for _, name := range mlImageNames {

			data = append(data, []string{
				name,
				strings.Join([]string{consts.MlImageDocsLink, name, consts.PipRequirementsFileName}, "/"),
			})
		}

		header = []string{"Image name", "Detail"}
	} else {

		mlImageInformation, err := gc.CreateMLImageInformationList(ctx, mlImageNames)
		if err != nil {
			return err
		}

		for _, name := range mlImageNames {

			data = append(data, []string{
				name,
				mlImageInformation[name].PackageVersions.AnacondaVersion,
				mlImageInformation[name].TensorflowGPUVersion,
				mlImageInformation[name].KerasVersion,
				mlImageInformation[name].TorchVersion,
				mlImageInformation[name].TorchVisionVersion,
				mlImageInformation[name].PackageVersions.ImageStatus,
			})
		}

		header = []string{"Image name", "Anaconda", "Tensorflow-GPU", "Keras", "Torch", "TorchVision", "Status"}
	}

	utils.CreateTable(header, data, stdOut)
	return nil
}
