/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package images

import (
	"bytes"
	"log"
	"os"
	"strings"
	"testing"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/testutils"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/utils"
)

const (
	simpleGetImages = `IMAGE NAME       	DETAIL                                                                                                        
cuda11.5.1-cudnn8	https://github.com/KindaiCVLAB/machinelearning-images/blob/master/versions/cuda11.5.1-cudnn8/requirements.txt	
cuda11.5.0-cudnn8	https://github.com/KindaiCVLAB/machinelearning-images/blob/master/versions/cuda11.5.0-cudnn8/requirements.txt	
cuda11.4.3-cudnn8	https://github.com/KindaiCVLAB/machinelearning-images/blob/master/versions/cuda11.4.3-cudnn8/requirements.txt	
cuda11.4.2-cudnn8	https://github.com/KindaiCVLAB/machinelearning-images/blob/master/versions/cuda11.4.2-cudnn8/requirements.txt	
cuda11.4.1-cudnn8	https://github.com/KindaiCVLAB/machinelearning-images/blob/master/versions/cuda11.4.1-cudnn8/requirements.txt	
cuda11.4.0-cudnn8	https://github.com/KindaiCVLAB/machinelearning-images/blob/master/versions/cuda11.4.0-cudnn8/requirements.txt	
cuda11.3.1-cudnn8	https://github.com/KindaiCVLAB/machinelearning-images/blob/master/versions/cuda11.3.1-cudnn8/requirements.txt	
cuda11.3.0-cudnn8	https://github.com/KindaiCVLAB/machinelearning-images/blob/master/versions/cuda11.3.0-cudnn8/requirements.txt	
cuda11.2.2-cudnn8	https://github.com/KindaiCVLAB/machinelearning-images/blob/master/versions/cuda11.2.2-cudnn8/requirements.txt	
cuda11.2.1-cudnn8	https://github.com/KindaiCVLAB/machinelearning-images/blob/master/versions/cuda11.2.1-cudnn8/requirements.txt	
cuda11.2.0-cudnn8	https://github.com/KindaiCVLAB/machinelearning-images/blob/master/versions/cuda11.2.0-cudnn8/requirements.txt	
cuda11.1.1-cudnn8	https://github.com/KindaiCVLAB/machinelearning-images/blob/master/versions/cuda11.1.1-cudnn8/requirements.txt	
cuda11.0.3-cudnn8	https://github.com/KindaiCVLAB/machinelearning-images/blob/master/versions/cuda11.0.3-cudnn8/requirements.txt	
cuda10.2-cudnn8  	https://github.com/KindaiCVLAB/machinelearning-images/blob/master/versions/cuda10.2-cudnn8/requirements.txt  	
cuda10.2-cudnn7  	https://github.com/KindaiCVLAB/machinelearning-images/blob/master/versions/cuda10.2-cudnn7/requirements.txt  	
`
	detailGetImages = `IMAGE NAME       	ANACONDA	TENSORFLOW-GPU	KERAS	TORCH 	TORCHVISION	STATUS             
cuda11.5.1-cudnn8	2021.11 	2.8.0         	2.8.0	1.11.0	0.12.0     	feature           	
cuda11.5.0-cudnn8	2021.11 	2.8.0         	2.8.0	1.11.0	0.12.0     	feature           	
cuda11.4.3-cudnn8	2021.11 	2.8.0         	2.8.0	1.11.0	0.12.0     	feature           	
cuda11.4.2-cudnn8	2021.11 	2.8.0         	2.8.0	1.11.0	0.12.0     	feature           	
cuda11.4.1-cudnn8	2021.11 	2.8.0         	2.8.0	1.11.0	0.12.0     	feature           	
cuda11.4.0-cudnn8	2021.11 	2.8.0         	2.8.0	1.11.0	0.12.0     	feature           	
cuda11.3.1-cudnn8	2021.11 	2.8.0         	2.8.0	1.11.0	0.12.0     	stable(pytorch)   	
cuda11.3.0-cudnn8	2021.05 	2.6.3         	2.6.0	1.10.2	0.11.3     	deprecated        	
cuda11.2.2-cudnn8	2021.11 	2.8.0         	2.8.0	1.11.0	0.12.0     	stable(tensorflow)	
cuda11.2.1-cudnn8	2021.05 	2.5.3         	2.4.3	1.9.0 	0.10.0     	deprecated        	
cuda11.2.0-cudnn8	2021.05 	2.5.3         	2.4.3	1.9.0 	0.10.0     	deprecated        	
cuda11.1.1-cudnn8	2021.05 	2.6.3         	2.6.0	1.9.1 	0.10.1     	deprecated        	
cuda11.0.3-cudnn8	2020.11 	2.5.3         	2.4.3	1.8.1 	0.9.1      	deprecated        	
cuda10.2-cudnn8  	2020.11 	2.5.3         	2.4.3	1.7.1 	0.8.2      	deprecated        	
cuda10.2-cudnn7  	2020.11 	2.5.3         	2.4.3	1.7.1 	0.8.2      	deprecated        	
`
)

func TestGetImages(t *testing.T) {
	testCases := []struct {
		name      string
		flags     map[string]string
		golden    string
		wantError bool
	}{
		{
			name:      "get the name and document link that available images",
			wantError: false,
			golden:    simpleGetImages,
		},
		{
			name:      "get the name and some library version that available images",
			flags:     map[string]string{"detail": "true"},
			wantError: false,
			golden:    detailGetImages,
		},
		{
			name:      "wrong option",
			flags:     map[string]string{"foo": "true"},
			wantError: true,
		},
	}

	token := os.Getenv("GITHUB_TOKEN")
	setUpTestToken(token)
	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			buf := &bytes.Buffer{}
			command := NewGetImageCmd(buf)
			err := testutils.StartFakeCommandWithOptions(test.flags, make([]string, 0), buf, command)

			if (err != nil) != test.wantError {
				t.Errorf("expected error: %v, got '%v'\n", test.wantError, err)
			} else if !strings.EqualFold(buf.String(), test.golden) {
				t.Errorf("does not match golden,\nWANT:\n%s\nGOT:\n%s\n", test.golden, buf.String())
			}
		})
	}
	cleanUpTestToken(token)
}

func setUpTestToken(token string) {
	if token != "" {
		tokenFilePath, err := utils.GetTokenFilePath()
		if err != nil {
			log.Fatal(err)
		}
		if err = os.WriteFile(tokenFilePath, []byte(token), 0600); err != nil {
			log.Fatal(err)
		}
	}
}

func cleanUpTestToken(token string) {
	tokenFilePath, err := utils.GetTokenFilePath()
	if err != nil {
		log.Fatal(err)
	}
	if utils.FileExists(tokenFilePath) && token != "" {
		if err = os.RemoveAll(tokenFilePath); err != nil {
			log.Fatal(err)
		}
	}
}
