/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package applications

import (
	"bytes"
	"strings"
	"testing"
	"time"

	imperatorconsts "github.com/tenzen-y/imperator/pkg/consts"
	cksoperatorv1beta2 "gitlab.com/kindaicvlab/cvcloud/cks-operator/api/v1beta2"
	cksoperatorconsts "gitlab.com/kindaicvlab/cvcloud/cks-operator/consts"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/testutils"
)

func TestGetApplications(t *testing.T) {

	const (
		testContainerImage = "registry.gitlab.com/kindaicvlab/cvcloud/machinelearning-images:cuda11.1.1-cudnn8"
	)

	var (
		deletionTime = metav1.Now()
	)

	machineLearning := &cksoperatorv1beta2.MachineLearning{
		TypeMeta: metav1.TypeMeta{
			APIVersion: cksoperatorv1beta2.GroupVersion.String(),
			Kind:       "MachineLearning",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: strings.Join([]string{
				"machinelearning",
				testutils.TestNameSpace,
			}, "-"),
			Namespace: testutils.TestNameSpace,
			Labels: map[string]string{
				imperatorconsts.MachineTypeKey: testutils.TestMachineName,
			},
		},
	}

	rookCephFSCSI := "rook-cephfs"
	cephfs := &corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name:      strings.Join([]string{testutils.TestNameSpace, "pvc"}, "-"),
			Namespace: testutils.TestNameSpace,
			Labels: map[string]string{
				cksoperatorconsts.CksStorageTypeLabelKey: cksoperatorconsts.CephFsLabel,
			},
		},
		Spec: corev1.PersistentVolumeClaimSpec{
			AccessModes: []corev1.PersistentVolumeAccessMode{
				corev1.ReadWriteMany,
			},
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					corev1.ResourceStorage: resource.MustParse(strings.Join([]string{
						testutils.TestStorageSize,
						"Gi",
					}, "")),
				},
			},
			StorageClassName: &rookCephFSCSI,
		},
		Status: corev1.PersistentVolumeClaimStatus{
			Phase: corev1.ClaimBound,
		},
	}

	codeServer := &corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name:      strings.Join([]string{cksoperatorv1beta2.CodeServer.String(), testutils.TestNameSpace}, "-"),
			Namespace: testutils.TestNameSpace,
			Labels: map[string]string{
				cksoperatorconsts.CksAppTypeLabelKey: cksoperatorv1beta2.CodeServer.String(),
				imperatorconsts.MachineTypeKey:       testutils.TestMachineName,
			},
			CreationTimestamp: metav1.NewTime(time.Now().Add(-(time.Hour + 15*time.Minute + 31*time.Second))),
		},
		Spec: corev1.PodSpec{
			Containers: []corev1.Container{
				{
					Image: testContainerImage,
				},
			},
		},
		Status: corev1.PodStatus{
			ContainerStatuses: []corev1.ContainerStatus{
				{
					State: corev1.ContainerState{
						Running: &corev1.ContainerStateRunning{
							StartedAt: metav1.Now(),
						},
					},
				},
			},
		},
	}

	jupyterLab := &corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name:      strings.Join([]string{cksoperatorv1beta2.JupyterLab.String(), testutils.TestNameSpace}, "-"),
			Namespace: testutils.TestNameSpace,
			Labels: map[string]string{
				cksoperatorconsts.CksAppTypeLabelKey: cksoperatorv1beta2.JupyterLab.String(),
				imperatorconsts.MachineTypeKey:       testutils.TestMachineName,
			},
			CreationTimestamp: metav1.NewTime(time.Now().Add(-26 * time.Hour)),
		},
		Spec: corev1.PodSpec{
			Containers: []corev1.Container{
				{
					Image: testContainerImage,
				},
			},
		},
		Status: corev1.PodStatus{
			ContainerStatuses: []corev1.ContainerStatus{
				{
					State: corev1.ContainerState{
						Running: &corev1.ContainerStateRunning{
							StartedAt: metav1.Now(),
						},
					},
				},
			},
		},
	}

	password := strings.Join([]string{testutils.TestNameSpace, "333"}, "")
	codeServerSecret := &corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name: strings.Join([]string{
				cksoperatorv1beta2.CodeServer.String(),
				testutils.TestNameSpace,
			}, "-"),
			Namespace: testutils.TestNameSpace,
		},
		Data: map[string][]byte{
			strings.Join([]string{
				cksoperatorv1beta2.CodeServer.String(),
				"pass",
			}, "-"): []byte(password),
		},
	}

	jupyterLabSecret := &corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name: strings.Join([]string{
				cksoperatorv1beta2.JupyterLab.String(),
				testutils.TestNameSpace,
			}, "-"),
			Namespace: testutils.TestNameSpace,
		},
		Data: map[string][]byte{
			strings.Join([]string{
				cksoperatorv1beta2.JupyterLab.String(),
				"pass",
			}, "-"): []byte(password),
		},
	}

	runningTestCases := []struct {
		name          string
		flags         map[string]string
		golden        string
		kubeResources []client.Object
		wantError     bool
	}{
		{
			name: "display a running code-server applications resource",
			golden: `TYPE      	MACHINE     	PASSWORD  	STORAGE	IMAGE            	LINK                             	STATUS 	AGE      
codeserver	spot-machine	yuki.tz333	100GiB 	cuda11.1.1-cudnn8	http://cvcloud.cks.local/yuki.tz/	Running	1h15m31s	
`,
			kubeResources: []client.Object{codeServer, machineLearning, codeServerSecret, cephfs},
			wantError:     false,
		},
		{
			name: "display a running jupyterlab applications resource",
			golden: `TYPE      	MACHINE     	PASSWORD  	STORAGE	IMAGE            	LINK                            	STATUS 	AGE  
jupyterlab	spot-machine	yuki.tz333	100GiB 	cuda11.1.1-cudnn8	http://cvcloud.cks.local/yuki.tz	Running	1d2h	
`,
			kubeResources: []client.Object{jupyterLab, machineLearning, jupyterLabSecret, cephfs},
			wantError:     false,
		},
		{
			name:          "codeserver-yuki.tz-secret was not found",
			kubeResources: []client.Object{codeServer, machineLearning, cephfs},
			wantError:     true,
		},
		{
			name: "there are not applications resource",
			golden: `TYPE	MACHINE	PASSWORD	STORAGE	IMAGE	LINK	STATUS	AGE 
`,
			kubeResources: []client.Object{cephfs},
			wantError:     false,
		},
		{
			name:      "bad option",
			flags:     map[string]string{"foo": "true"},
			wantError: true,
		},
	}

	for _, test := range runningTestCases {
		t.Run(test.name, func(t *testing.T) {
			c, err := testutils.NewFakeKubeClient(test.kubeResources)
			if err != nil {
				t.Fatal(err)
			}
			buf := &bytes.Buffer{}
			command := NewGetAppCmd(c, buf)
			err = testutils.StartFakeCommandWithOptions(test.flags, make([]string, 0), buf, command)

			if (err != nil) != test.wantError {
				t.Errorf("expected error: %v, got '%v'\n", test.wantError, err)
			} else if !strings.EqualFold(buf.String(), test.golden) {
				t.Errorf("does not match golden,\nWANT:\n%s\nGOT:\n%s\n", test.golden, buf.String())
			}
		})
	}

	crashedContainerStatus := []corev1.ContainerStatus{
		{
			State: corev1.ContainerState{
				Waiting: &corev1.ContainerStateWaiting{
					Reason: "CrashLoopBackOff",
				},
			},
		},
	}

	emptyContainerStatus := []corev1.ContainerStatus{
		{
			State: corev1.ContainerState{},
		},
	}

	creatingContainerStatus := []corev1.ContainerStatus{
		{
			State: corev1.ContainerState{
				Waiting: &corev1.ContainerStateWaiting{
					Reason: "ContainerCreating",
				},
			},
		},
	}

	// Crashed Pod
	crashedCodeServer := *codeServer
	crashedCodeServer.Status.ContainerStatuses = crashedContainerStatus

	crashedJupyterLab := *jupyterLab
	crashedJupyterLab.Status.ContainerStatuses = crashedContainerStatus

	// Empty Status Pod
	emptyCodeServer := *codeServer
	emptyCodeServer.Status.ContainerStatuses = emptyContainerStatus

	emptyJupyterLab := *jupyterLab
	emptyJupyterLab.Status.ContainerStatuses = emptyContainerStatus

	// Creating Pod
	creatingCodeServer := *codeServer
	creatingCodeServer.Status.ContainerStatuses = creatingContainerStatus

	creatingJupyterLab := *jupyterLab
	creatingJupyterLab.Status.ContainerStatuses = creatingContainerStatus

	waitingTestCases := []struct {
		name          string
		golden        string
		kubeResources []client.Object
	}{
		{
			name: "display crashed code-server applications resource",
			golden: `TYPE      	MACHINE     	PASSWORD  	STORAGE	IMAGE            	LINK                             	STATUS 	AGE      
codeserver	spot-machine	yuki.tz333	100GiB 	cuda11.1.1-cudnn8	http://cvcloud.cks.local/yuki.tz/	Crashed	1h15m31s	
`,
			kubeResources: []client.Object{machineLearning, &crashedCodeServer, cephfs, codeServerSecret},
		},
		{
			name: "display crashed jupyterlab applications resource",
			golden: `TYPE      	MACHINE     	PASSWORD  	STORAGE	IMAGE            	LINK                            	STATUS 	AGE  
jupyterlab	spot-machine	yuki.tz333	100GiB 	cuda11.1.1-cudnn8	http://cvcloud.cks.local/yuki.tz	Crashed	1d2h	
`,
			kubeResources: []client.Object{machineLearning, &crashedJupyterLab, cephfs, jupyterLabSecret},
		},
		{
			name: "display empty status code-server applications resource",
			golden: `TYPE      	MACHINE     	PASSWORD  	STORAGE	IMAGE            	LINK                             	STATUS 	AGE      
codeserver	spot-machine	yuki.tz333	100GiB 	cuda11.1.1-cudnn8	http://cvcloud.cks.local/yuki.tz/	Crashed	1h15m31s	
`,
			kubeResources: []client.Object{machineLearning, &emptyCodeServer, cephfs, codeServerSecret},
		},
		{
			name: "display empty status jupyterlab applications resource",
			golden: `TYPE      	MACHINE     	PASSWORD  	STORAGE	IMAGE            	LINK                            	STATUS 	AGE  
jupyterlab	spot-machine	yuki.tz333	100GiB 	cuda11.1.1-cudnn8	http://cvcloud.cks.local/yuki.tz	Crashed	1d2h	
`,
			kubeResources: []client.Object{machineLearning, &emptyJupyterLab, cephfs, jupyterLabSecret},
		},
		{
			name: "display creating code-server applications resource",
			golden: `TYPE      	MACHINE     	PASSWORD  	STORAGE	IMAGE            	LINK                             	STATUS  	AGE      
codeserver	spot-machine	yuki.tz333	100GiB 	cuda11.1.1-cudnn8	http://cvcloud.cks.local/yuki.tz/	Creating	1h15m31s	
`,
			kubeResources: []client.Object{machineLearning, &creatingCodeServer, cephfs, codeServerSecret},
		},
		{
			name: "display creating jupyterlab applications resource",
			golden: `TYPE      	MACHINE     	PASSWORD  	STORAGE	IMAGE            	LINK                            	STATUS  	AGE  
jupyterlab	spot-machine	yuki.tz333	100GiB 	cuda11.1.1-cudnn8	http://cvcloud.cks.local/yuki.tz	Creating	1d2h	
`,
			kubeResources: []client.Object{machineLearning, &creatingJupyterLab, cephfs, jupyterLabSecret},
		},
	}

	for _, test := range waitingTestCases {
		t.Run(test.name, func(t *testing.T) {
			c, err := testutils.NewFakeKubeClient(test.kubeResources)
			if err != nil {
				t.Fatal(err)
			}
			buf := &bytes.Buffer{}
			command := NewGetAppCmd(c, buf)
			if err = testutils.StartFakeCommandWithOptions(make(map[string]string), make([]string, 0), buf, command); err != nil {
				t.Fatal(err)
			}

			if !strings.EqualFold(buf.String(), test.golden) {
				t.Errorf("does not match golden,\nWANT:\n%s\nGOT:\n%s\n", test.golden, buf.String())
			}
		})
	}

	errorContainerStatus := []corev1.ContainerStatus{
		{
			State: corev1.ContainerState{
				Terminated: &corev1.ContainerStateTerminated{
					Reason: "Error",
				},
			},
		},
	}

	terminatedContainerStatus := []corev1.ContainerStatus{
		{
			State: corev1.ContainerState{
				Terminated: &corev1.ContainerStateTerminated{
					Reason: "Terminated",
				},
			},
		},
	}

	// Error Pod
	errorCodeServer := *codeServer
	errorCodeServer.Status.ContainerStatuses = errorContainerStatus

	errorJupyterLab := *jupyterLab
	errorJupyterLab.Status.ContainerStatuses = errorContainerStatus

	// Creating Pod
	terminatedCodeServer := *codeServer
	terminatedCodeServer.Status.ContainerStatuses = terminatedContainerStatus

	terminatedJupyterLab := *jupyterLab
	terminatedJupyterLab.Status.ContainerStatuses = terminatedContainerStatus

	terminatedTestCases := []struct {
		name          string
		golden        string
		kubeResources []client.Object
	}{
		{
			name: "display error code-server applications resource",
			golden: `TYPE      	MACHINE     	PASSWORD  	STORAGE	IMAGE            	LINK                             	STATUS 	AGE      
codeserver	spot-machine	yuki.tz333	100GiB 	cuda11.1.1-cudnn8	http://cvcloud.cks.local/yuki.tz/	Crashed	1h15m31s	
`,
			kubeResources: []client.Object{machineLearning, &errorCodeServer, cephfs, codeServerSecret},
		},
		{
			name: "display error jupyterlab applications resource",
			golden: `TYPE      	MACHINE     	PASSWORD  	STORAGE	IMAGE            	LINK                            	STATUS 	AGE  
jupyterlab	spot-machine	yuki.tz333	100GiB 	cuda11.1.1-cudnn8	http://cvcloud.cks.local/yuki.tz	Crashed	1d2h	
`,
			kubeResources: []client.Object{machineLearning, &errorJupyterLab, cephfs, jupyterLabSecret},
		},
		{
			name: "display terminated code-server applications resource",
			golden: `TYPE      	MACHINE     	PASSWORD  	STORAGE	IMAGE            	LINK                             	STATUS    	AGE      
codeserver	spot-machine	yuki.tz333	100GiB 	cuda11.1.1-cudnn8	http://cvcloud.cks.local/yuki.tz/	Terminated	1h15m31s	
`,
			kubeResources: []client.Object{machineLearning, &terminatedCodeServer, cephfs, codeServerSecret},
		},
		{
			name: "display terminated jupyterlab applications resource",
			golden: `TYPE      	MACHINE     	PASSWORD  	STORAGE	IMAGE            	LINK                            	STATUS    	AGE  
jupyterlab	spot-machine	yuki.tz333	100GiB 	cuda11.1.1-cudnn8	http://cvcloud.cks.local/yuki.tz	Terminated	1d2h	
`,
			kubeResources: []client.Object{machineLearning, &terminatedJupyterLab, cephfs, jupyterLabSecret},
		},
	}

	for _, test := range terminatedTestCases {
		t.Run(test.name, func(t *testing.T) {
			c, err := testutils.NewFakeKubeClient(test.kubeResources)
			if err != nil {
				t.Fatal(err)
			}
			buf := &bytes.Buffer{}
			command := NewGetAppCmd(c, buf)
			if err = testutils.StartFakeCommandWithOptions(make(map[string]string), make([]string, 0), buf, command); err != nil {
				t.Fatal(err)
			}

			if !strings.EqualFold(buf.String(), test.golden) {
				t.Errorf("does not match golden,\nWANT:\n%s\nGOT:\n%s\n", test.golden, buf.String())
			}
		})
	}

	// Terminating Pod
	terminatingCodeServer := *codeServer
	terminatingCodeServer.ObjectMeta.DeletionTimestamp = &deletionTime

	terminatingJupyterLab := *jupyterLab
	terminatingJupyterLab.ObjectMeta.DeletionTimestamp = &deletionTime

	terminatingTestCases := []struct {
		name          string
		golden        string
		kubeResources []client.Object
	}{
		{
			name: "display terminating code-server applications resource",
			golden: `TYPE      	MACHINE     	PASSWORD	STORAGE	IMAGE            	LINK   	STATUS     	AGE         
codeserver	spot-machine	Deleted 	100GiB 	cuda11.1.1-cudnn8	Deleted	Terminating	Terminating	
`,
			kubeResources: []client.Object{machineLearning, &terminatingCodeServer, cephfs, codeServerSecret},
		},
		{
			name: "display terminating jupyterlab applications resource",
			golden: `TYPE      	MACHINE     	PASSWORD	STORAGE	IMAGE            	LINK   	STATUS     	AGE         
jupyterlab	spot-machine	Deleted 	100GiB 	cuda11.1.1-cudnn8	Deleted	Terminating	Terminating	
`,
			kubeResources: []client.Object{machineLearning, &terminatingJupyterLab, cephfs, jupyterLabSecret},
		},
	}

	for _, test := range terminatingTestCases {
		t.Run(test.name, func(t *testing.T) {
			c, err := testutils.NewFakeKubeClient(test.kubeResources)
			if err != nil {
				t.Fatal(err)
			}
			buf := &bytes.Buffer{}
			command := NewGetAppCmd(c, buf)
			if err = testutils.StartFakeCommandWithOptions(make(map[string]string), make([]string, 0), buf, command); err != nil {
				t.Fatal(err)
			}

			if !strings.EqualFold(buf.String(), test.golden) {
				t.Errorf("does not match golden,\nWANT:\n%s\nGOT:\n%s\n", test.golden, buf.String())
			}
		})
	}

}
