/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package applications

import (
	"io"
	"strconv"
	"strings"
	"time"

	"github.com/spf13/cobra"
	imperatorconsts "github.com/tenzen-y/imperator/pkg/consts"
	cksoperatorv1beta2 "gitlab.com/kindaicvlab/cvcloud/cks-operator/api/v1beta2"
	corev1 "k8s.io/api/core/v1"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/consts"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/kubectl"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/utils"
)

type getAppOptions struct{}

const getAppDescription = `
The command gets your running applications.
`

func NewGetAppCmd(client *kubectl.Client, stdOut io.Writer) *cobra.Command {

	o := &getAppOptions{}

	cmd := &cobra.Command{
		Use:          "applications",
		Aliases:      consts.ApplicationsAlias,
		Short:        "get your running applications",
		Long:         getAppDescription,
		SilenceUsage: true,
		RunE: func(cmd *cobra.Command, args []string) error {
			return o.run(client, stdOut)
		},
	}

	return cmd
}

func (o *getAppOptions) run(client *kubectl.Client, stdOut io.Writer) error {

	var (
		data           [][]string
		codeserverPods corev1.PodList
		jupyterlabPods corev1.PodList
	)

	if err := client.ListCodeServerPods(&codeserverPods); err != nil {
		return err
	}
	if err := client.ListJupyterLabPods(&jupyterlabPods); err != nil {
		return err
	}

	// JupyterLab
	if len(jupyterlabPods.Items) > 0 {
		if err := getApp(client, cksoperatorv1beta2.JupyterLab.String(), jupyterlabPods, &data); err != nil {
			return err
		}
	}
	// CodeServer
	if len(codeserverPods.Items) > 0 {
		if err := getApp(client, cksoperatorv1beta2.CodeServer.String(), codeserverPods, &data); err != nil {
			return err
		}
	}

	header := []string{"Type", "Machine", "Password", "Storage", "Image", "Link", "Status", "Age"}
	utils.CreateTable(header, data, stdOut)
	return nil
}

const (
	//cvcloudLink is url for all applications
	cvcloudLink = "http://cvcloud.cks.local/"
)

func getApp(client *kubectl.Client, appType string, pods corev1.PodList, data *[][]string) error {

	var (
		secretName = strings.Join([]string{appType, client.GetNamespace()}, "-")
		secret     corev1.Secret
		password   string
		podAge     string
		podStatus  string
		link       string
		pvc        corev1.PersistentVolumeClaim
	)

	// Persistent Volume Claim
	if err := client.GetCephFSPVC(&pvc); err != nil {
		return err
	}

	for _, pod := range pods.Items {

		if pod.GetObjectMeta().GetDeletionTimestamp().IsZero() {

			if len(pod.Status.ContainerStatuses) == 0 {
				podStatus = "Creating"
			} else {
				containerStatus := pod.Status.ContainerStatuses[0].State
				switch {
				case containerStatus.Running != nil:
					podStatus = "Running"
				case containerStatus.Waiting != nil:
					if containerStatus.Waiting.Reason == "CrashLoopBackOff" {
						podStatus = "Crashed"
					} else {
						podStatus = "Creating"
					}
				case containerStatus.Terminated != nil:
					if containerStatus.Terminated.Reason == "Error" {
						podStatus = "Crashed"
					} else {
						podStatus = "Terminated"
					}
				default:
					podStatus = "Crashed"
				}
			}

			if err := client.GetResources(&secret, secretName); err != nil {
				return err
			}
			// get password
			password = string(secret.Data[strings.Join([]string{appType, "pass"}, "-")])
			podAge = getPodAge(pod)

			switch cksoperatorv1beta2.ApplicationType(appType) {
			case cksoperatorv1beta2.JupyterLab:
				link = strings.Join([]string{cvcloudLink, client.GetNamespace()}, "")
			case cksoperatorv1beta2.CodeServer:
				link = strings.Join([]string{cvcloudLink, client.GetNamespace(), "/"}, "")
			}

		} else {
			podStatus = "Terminating"
			password = "Deleted"
			link = "Deleted"
			podAge = "Terminating"
		}

		*data = append(*data, []string{
			appType,
			pod.ObjectMeta.Labels[imperatorconsts.MachineTypeKey],
			password,
			strings.Join([]string{pvc.Spec.Resources.Requests.Storage().String(), "B"}, ""),
			strings.Split(pod.Spec.Containers[0].Image, ":")[1],
			link,
			podStatus,
			podAge,
		})
	}

	return nil
}

func getPodAge(pod corev1.Pod) string {
	runningTime := time.Since(pod.GetCreationTimestamp().Time)

	if uint64(runningTime.Hours()/24) != 0 {
		runningDay := uint64(runningTime.Hours() / 24)
		runningHours := uint64(runningTime.Hours()) - 24*runningDay
		return strconv.FormatUint(runningDay, 10) + "d" + strconv.FormatUint(runningHours, 10) + "h"
	}

	return runningTime.Truncate(time.Second).String()
}
