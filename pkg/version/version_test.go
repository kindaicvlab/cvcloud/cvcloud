/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package version

import (
	"bytes"
	"strings"
	"testing"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/testutils"
)

func TestVersionHelpDescription(t *testing.T) {
	testCases := []struct {
		name      string
		args      []string
		wantError bool
		golden    string
	}{
		{
			name: "display cvcloud version when executing only version command",
			golden: `dev
`,
			wantError: false,
		},
		{
			name:      "display help description when executing not exist get subcommand",
			args:      []string{"foo"},
			wantError: true,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			buf := &bytes.Buffer{}
			command := NewVersionCmd(buf)
			err := testutils.StartFakeCommandWithOptions(make(map[string]string), test.args, buf, command)

			if (err != nil) != test.wantError {
				t.Errorf("expected error: %v, got '%v'\n", test.wantError, err)
			} else if !strings.EqualFold(buf.String(), test.golden) {
				t.Errorf("does not match golden,\nWANT:\n%s\nGOT:\n%s\n", test.golden, buf.String())
			}
		})
	}

}
