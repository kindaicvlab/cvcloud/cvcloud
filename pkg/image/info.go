/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package image

import (
	"encoding/json"
	"regexp"
	"strings"
)

// MLImageInfo is python library version installed in machinelearning-images
type MLImageInfo struct {
	//PackageVersions is version of packages
	PackageVersions PackageVersions
	//TensorflowGPUVersion is version for tensorflow-gpu
	TensorflowGPUVersion string
	//KerasVersion is version for keras
	KerasVersion string
	//TorchVersion is version for torch
	TorchVersion string
	//TorchVisionVersion is version for torch vision
	TorchVisionVersion string
	//DocsLink is the hyperlink to see a document written about a specific container image version.
	DocsLink string
	//RegistryName is container registry name
	RegistryName string
}

type PackageVersions struct {
	ImageStatus     string `json:"IMAGE_STATUS"`
	AnacondaVersion string `json:"ANACONDA_VERSION"`
}

func (m *MLImageInfo) parsePackageVersions(jsonData []byte) error {
	if err := json.Unmarshal(jsonData, &m.PackageVersions); err != nil {
		return err
	}
	return nil
}

func (m *MLImageInfo) parsePipRequirements(pipRequirements string) error {
	displayLibraries := map[string]*string{
		"tensorflow-gpu": &m.TensorflowGPUVersion,
		"keras":          &m.KerasVersion,
		"torch":          &m.TorchVersion,
		"torchvision":    &m.TorchVisionVersion,
		"tf-nightly-gpu": &m.TensorflowGPUVersion,
	}

	for _, line := range regexp.MustCompile(`\r\n|\n\r|\n|\r`).Split(pipRequirements, -1) {
		if line == "" {
			continue
		}
		libraryInfo := strings.Split(line, "==")
		if len(libraryInfo) != 2 {
			continue
		} else if _, exist := displayLibraries[libraryInfo[0]]; !exist {
			continue
		}
		*displayLibraries[libraryInfo[0]] = libraryInfo[1]
	}

	if m.PackageVersions.ImageStatus == "feature" {
		if m.TorchVersion == "" {
			m.TorchVersion = "nightly"
		}
		if m.TorchVisionVersion == "" {
			m.TorchVisionVersion = "nightly"
		}
	}

	return nil
}
