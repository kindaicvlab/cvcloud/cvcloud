/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package image

import (
	"sort"
	"strings"

	goversion "github.com/hashicorp/go-version"
)

func sortImageName(mlImageNameSlice []string) ([]string, error) {

	var versionsRaw []string
	for _, name := range mlImageNameSlice {

		splitCudaCudnn := strings.Split(name, "-")
		cudaVersion := strings.Trim(splitCudaCudnn[0], "cuda")
		cudnnVersion := strings.Trim(splitCudaCudnn[1], "cudn" /*SA1024*/)

		// when patch version do not exist
		if len(strings.Split(cudaVersion, ".")) == 2 {
			cudaVersion = strings.Join([]string{cudaVersion, "0"}, ".")
		}

		versionsRaw = append(versionsRaw, strings.Join([]string{cudaVersion, cudnnVersion}, "."))
	}

	versions := make([]*goversion.Version, len(versionsRaw))
	for i, raw := range versionsRaw {
		v, err := goversion.NewVersion(raw)
		if err != nil {
			return mlImageNameSlice, err
		}
		versions[i] = v
	}
	sort.Sort(sort.Reverse(goversion.Collection(versions)))

	var result []string
	for _, sortedV := range versions {
		splitCudaCudnnVersion := strings.Split(sortedV.String(), ".")

		cudaVersion := strings.Join(splitCudaCudnnVersion[:3], ".")
		if cudaVersion == "10.2.0" {
			cudaVersion = strings.Join(splitCudaCudnnVersion[:2], ".")
		}
		cudnnVersion := splitCudaCudnnVersion[3]

		imageName := strings.Join([]string{
			strings.Join([]string{"cuda", cudaVersion}, ""),
			strings.Join([]string{"cudnn", cudnnVersion}, "")},
			"-")
		result = append(result, imageName)
	}

	return result, nil
}
