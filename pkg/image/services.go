/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package image

import (
	"context"
	"regexp"
	"strings"
	"sync"

	"github.com/google/go-github/v41/github"
	"golang.org/x/sync/errgroup"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/consts"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/utils"
)

//CreateMLImageNameList is super set for GetImageName(ctx context.Context)
func (g *GitHub) CreateMLImageNameList(ctx context.Context) ([]string, error) {
	return g.getImageName(ctx)
}

//getImageName will return image name list
func (g *GitHub) getImageName(ctx context.Context) ([]string, error) {

	var mlImageNameList []string

	repo := g.Client.Repositories
	_, dir, _, err := repo.GetContents(ctx, repositoryOwner, repositoryName, versionFileBaseDir, &github.RepositoryContentGetOptions{Ref: branch})

	if err != nil {
		return mlImageNameList, err
	}

	r := regexp.MustCompile(consts.VersionDirectoryNameRegexpPattern)
	for _, content := range dir {
		matches := r.FindAllString(content.GetName(), -1)
		if content.GetType() != "dir" || len(matches) != 1 {
			continue
		}
		mlImageNameList = append(mlImageNameList, content.GetName())
	}

	return sortImageName(mlImageNameList)
}

//CreateMLImageInformationList will return completely image information
func (g *GitHub) CreateMLImageInformationList(parent context.Context, mlImageNameSlice []string) (map[string]*MLImageInfo, error) {
	eg, ctx := errgroup.WithContext(parent)
	mlImageInformation := map[string]*MLImageInfo{}
	mutex := &sync.Mutex{}

	for _, imageName := range mlImageNameSlice {
		name := imageName
		eg.Go(func() error {
			select {
			case <-ctx.Done():
				return nil
			default:
				mlImageDetail, err := g.GetMLImageLibraryVersion(ctx, name)
				mutex.Lock()
				mlImageInformation[name] = mlImageDetail
				mutex.Unlock()
				if err != nil {
					return err
				}
				return nil
			}
		})
	}
	if err := eg.Wait(); err != nil {
		return mlImageInformation, err
	}
	return mlImageInformation, nil
}

//GetMLImageLibraryVersion will return python library version
func (g *GitHub) GetMLImageLibraryVersion(ctx context.Context, imageName string) (*MLImageInfo, error) {

	repo := g.Client.Repositories

	// init MLImageInfo
	mlImageInfo := &MLImageInfo{
		RegistryName: utils.GetRegistryNameTag(imageName),
	}

	// versions/IMAGE_NAME/packages.json
	packageVersionPath := strings.Join([]string{
		versionFileBaseDir,
		imageName,
		"packages.json",
	}, "/")
	packageFile, _, _, err := repo.GetContents(ctx, repositoryOwner, repositoryName, packageVersionPath, &github.RepositoryContentGetOptions{Ref: branch})
	if err != nil {
		return &MLImageInfo{}, err
	}
	packageRawData, err := packageFile.GetContent()
	if err != nil {
		return &MLImageInfo{}, err
	}
	if err = mlImageInfo.parsePackageVersions([]byte(packageRawData)); err != nil {
		return &MLImageInfo{}, err
	}

	// versions/IMAGE_NAME/requirements.txt
	pipRequirementsPath := strings.Join([]string{
		versionFileBaseDir,
		imageName,
		consts.PipRequirementsFileName,
	}, "/")
	pipRequirementsFile, _, _, err := repo.GetContents(ctx, repositoryOwner, repositoryName, pipRequirementsPath, &github.RepositoryContentGetOptions{Ref: branch})
	if err != nil {
		return &MLImageInfo{}, err
	}
	pipRequirementsRawData, err := pipRequirementsFile.GetContent()
	if err != nil {
		return &MLImageInfo{}, err
	}
	if err = mlImageInfo.parsePipRequirements(pipRequirementsRawData); err != nil {
		return &MLImageInfo{}, err
	}

	return mlImageInfo, nil
}
