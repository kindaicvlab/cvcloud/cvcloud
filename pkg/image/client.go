/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

//Package image get available container images
package image

import (
	"context"
	"os"

	"github.com/google/go-github/v41/github"
	"golang.org/x/oauth2"

	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/utils"
)

const (
	versionFileBaseDir = "versions"
	repositoryOwner    = "KindaiCVLAB"
	repositoryName     = "machinelearning-images"
	branch             = "master"
)

type GitHub struct {
	Client *github.Client
}

func NewGitHubClient(ctx context.Context) (*GitHub, error) {
	tokenFilePath, err := utils.GetTokenFilePath()
	if err != nil {
		return &GitHub{}, err
	}
	if !utils.FileExists(tokenFilePath) {
		return &GitHub{
			Client: github.NewClient(nil),
		}, nil
	}

	token, err := os.ReadFile(tokenFilePath)
	if err != nil {
		return &GitHub{}, err
	}
	ts := oauth2.StaticTokenSource(
		&oauth2.Token{AccessToken: string(token)},
	)
	tc := oauth2.NewClient(ctx, ts)

	return &GitHub{
		Client: github.NewClient(tc),
	}, nil
}
