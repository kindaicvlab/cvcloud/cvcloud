/*
Copyright © 2020 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package cvcloud

import (
	"github.com/spf13/cobra"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/create"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/delete"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/get"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/initialize"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/update"
	"gitlab.com/kindaicvlab/cvcloud/cvcloud/pkg/version"
	"io"
)

const rootDescription = `
Client Tool for CVCloud
`

//NewRootCmd is root
func NewRootCmd(stdOut io.Writer) (*cobra.Command, error) {
	cmd := &cobra.Command{
		Use:          "cvcloud",
		Short:        rootDescription,
		Long:         rootDescription,
		SilenceUsage: true,
	}

	cmd.AddCommand(initialize.NewInitCmd(stdOut))
	cmd.AddCommand(get.NewGetCmd(stdOut))
	cmd.AddCommand(delete.NewDeleteCmd(stdOut))
	cmd.AddCommand(create.NewCreateCmd(stdOut))
	cmd.AddCommand(update.NewUpdateCmd(stdOut))
	cmd.AddCommand(version.NewVersionCmd(stdOut))

	return cmd, nil
}
