 # v3 向けのデザインドキュメント

## 概要

- v3 では K8s カスタムコントローラによって管理された計算リソースを要求することにする．
- それによって残リソースの提示なども cvcloud で行う．
- v2 は docker コンテナで配布していたが，v3 では homebrew での配布も行う．
- アクセスコントロールは kubeconfig を用いる．
- マニフェストは個人 Git リポジトリではなく共通リポジトリを用意する．
- v3 ではインタラクティブモードを廃止し，引数で命令を実行する形式にする．
- argocd を廃止し，kubectl ベースでコントロールを行う．

## 操作

- machines: マシンタイプ
- images: 利用可能なコンテナイメージ
- apps: jupyter や code など
- storage: Persistent Volume
- data: Persistent Volume 上のデータ

|Operation|      Syntax      |          Description            |
|:-------:|:----------------:|:-------------------------------:|
|   ```get```   | ```cvcloud get [machines \| apps \| images \| storage]``` |情報を表示する．|
|  ```create``` | ```cvcloud create [app --machine <Machine name> --image <image name> --type [jupyterlab \| codeserver] \| storage --size <storage size>] ```|作成する．|
| ```update```  | ```cvcloud update [storage --size <storage size>\| image \| type]```|ストレージ容量・コンテナイメージ・アプリケーションタイプを変更する．|
| ```delete```  | ```cvcloud delete [app --type [jupyter \| code] \| storage]```|ストレージ・アプリケーションを削除する．|
| ```upload```  | ```cvcloud upload data --local <local data directory>``` |手元のデータを Persistent Volume へアップロードする．|
|  ```show```   | ```cvcloud show data```| Persistent Volume 上のデータを表示する．|
|```download``` | ```cvcloud download data --remote <remote data directory> --local <localdata directry>``` | Persistent Volume 上のデータをダウンロードする．|

## ラベル

- Apps

|Type|Label|
|:---:|:---:|
|JupyterLab|cks-apps=jupyterlab|
|Code Server|cks-apps=code-server|

- Storage

|Type|Label|
|:---:|:---:|
|cephfs|cks-storage=cephfs|
|cks-managed-nfs-storage|cks-storage=nfs|

- Machine

|  Machine Name |          Label            |
|:--------------:|:-------------------------:|
|   vram-large1  |  cks-Machine=vram-large1 |
|   vram-large2  |  cks-Machine=vram-large2 |
| compute-midium |cks-Machine=compute-midium|
| compute-large  | cks-Machine=compute-large|

## インスタンスサイズ

- Machine Type

|  Machine Name   | cpu| memory|   GPU  |number of GPU| number of Machine | dependence  |
|:----------------:|:--:|:-----:|:------:|:-----------:|:------------------:|:-----------:|
|    vram-large1   | 12 |  64GB | pascal |      2      |         2          | vram-large2 |
|    vram-large2   | 64 | 300GB | pascal |      4      |         1          | vram-large1 |
|  compute-midium  | 12 |  64GB | turing |      2      |         3          |     N/A     |
|   compute-large  | 20 |  64GB | ampere |      2      |         1          |     N/A     |

- Machine Spec

| |GPU|
|:---:|:---:|
|__pascal__|- Nvidia Quadro GP100|
|__turing__|- Nvidia Geforce RTX2080Ti <br> - Nvidia Quadro RTX8000|
|__ampere__|- Nvidia Geforce RTX3080|

