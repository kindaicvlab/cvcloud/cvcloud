# ユーザガイド

cvcloud コマンドは研究室ネットワークに接続されている場合のみ正常に動作します．
研究室外からアクセスする場合は [sshuttle](https://github.com/sshuttle/sshuttle) などを利用して研究室ネットワークにアクセスした状態にしてから利用する必要があります．

- 目次
    - [cvcloud コマンドのインストール方法](https://gitlab.com/kindaicvlab/cvcloud/cvcloud/-/blob/master/docs/userguide.md#%E3%82%A4%E3%83%B3%E3%82%B9%E3%83%88%E3%83%BC%E3%83%AB%E6%96%B9%E6%B3%95)
    - cvcloud を初めて利用する人へ
        - [cvcloud コマンドの初期化](https://gitlab.com/kindaicvlab/cvcloud/cvcloud/-/blob/master/docs/userguide.md#%E5%88%9D%E6%9C%9F%E5%8C%96)
        - [cvcloud の概要](https://gitlab.com/kindaicvlab/cvcloud/cvcloud/-/blob/master/docs/userguide.md#%E4%BD%BF%E7%94%A8%E6%96%B9%E6%B3%95)  
        - [cvcloud 上で永続ストレージを作成する．](https://gitlab.com/kindaicvlab/cvcloud/cvcloud/-/blob/master/docs/userguide.md#storages-%E3%82%92%E4%BD%9C%E6%88%90%E3%81%99%E3%82%8B%E5%88%9D%E3%82%81%E3%81%A6%E5%88%A9%E7%94%A8%E3%81%99%E3%82%8B%E5%A0%B4%E5%90%88%E3%81%AE%E3%81%BF)
    - cvcloud コマンドの使い方
        - cvcloud 利用前
            - [空いている machines を探す](https://gitlab.com/kindaicvlab/cvcloud/cvcloud/-/blob/master/docs/userguide.md#%E7%A9%BA%E3%81%84%E3%81%A6%E3%81%84%E3%82%8B-machines-%E3%82%92%E6%8E%A2%E3%81%99)
            - [利用できるイメージを探す](https://gitlab.com/kindaicvlab/cvcloud/cvcloud/-/blob/master/docs/userguide.md#%E5%88%A9%E7%94%A8%E3%81%99%E3%82%8B-images-%E3%82%92%E6%B1%BA%E3%82%81%E3%82%8B)
            - [IDE アプリケーションを作成する](https://gitlab.com/kindaicvlab/cvcloud/cvcloud/-/blob/master/docs/userguide.md#applications-%E3%82%92%E4%BD%9C%E6%88%90%E3%81%99%E3%82%8B)
        - cvcloud 利用後
            - [IDE アプリケーションを削除する](https://gitlab.com/kindaicvlab/cvcloud/cvcloud/-/blob/master/docs/userguide.md#applications-%E3%82%92%E5%89%8A%E9%99%A4%E3%81%99%E3%82%8B)
        - その他のシチュエーション
            - [永続ストレージを増やしたい](https://gitlab.com/kindaicvlab/cvcloud/cvcloud/-/blob/master/docs/userguide.md#storages-%E5%AE%B9%E9%87%8F%E3%81%AE%E8%BF%BD%E5%8A%A0)
            - [(危険)永続ストレージを削除したい](https://gitlab.com/kindaicvlab/cvcloud/cvcloud/-/blob/master/docs/userguide.md#storages-%E3%81%AE%E5%89%8A%E9%99%A4)

# インストール方法

## MacOS or Linux

Homebrew でインストールする方法とインストールスクリプトを利用する方法を用意しています．

### Homebrew 

[Homebrew](https://brew.sh/index_ja) を用いて配布を行っています．
Homebrew をインストールしていない場合は公式キュメントにしたがってインストールしてください．

- 初めてインストールする場合

```shell
$ brew tap cvcloud/cvcloud https://gitlab.com/kindaicvlab/cvcloud/homebrew-cvcloud.git
$ brew install cvcloud
```

- 以前にインストール済みで，最新バージョンへアップデートする場合

```shell
$ brew upgrade cvcloud
```

### インストールスクリプト

バイナリファイルダウンロード用スクリプトを用意しています．スクリプト内で `jq` を利用するため事前インストールする必要があります．
Ubuntu の場合は `sudo apt-get install -y jq` でインストールすることができます．

以下のコマンドでインストールした後， PATH の通っている場所へバイナリファイルを移動する必要があります．
以下の例では，`/usr/local/bin` へ移動させています．

```shell
# cvcloud コマンドをインストールする
$ curl -s https://gitlab.com/kindaicvlab/cvcloud/cvcloud/-/raw/master/hack/install_cvcloud.sh | bash
# PATH の通っている場所へ移動する
$ sudo mv cvcloud /usr/local/bin
```

## Windows
Release ページで Windows 用のバイナリを配布しています．ダウンロードしてパスの通っている場所に配置してください．

[Release ページ](https://gitlab.com/kindaicvlab/cvcloud/cvcloud/-/releases)


## 自動補完
`cvcloud ` コマンドには自動補完機能がついています．bash, zsh, fish, powershell で用意しているので，
利用しているシェルに合わせてコマンドを実行してください．以下は bash，zsh，fish を使用している場合の例です．

- bash
- Note: Bash バージョン 5 未満は使用できません．

```shell
$ # OS 共通
$ echo "source <(cvcloud completion bash)" >> ~/.bashrc
$ # MacOS
$ cvcloud completion bash > /usr/local/etc/bash_completion.d/cvcloud
$ # Linux
$ cvcloud completion bash > /etc/bash_completion.d/cvcloud
```

- zsh

```shell
% # zsh の補完設定を行っていない場合は以下を実行する．
% echo "autoload -U compinit; compinit" >> ~/.zshrc
% # 以下は必ず実行する．
% cvcloud completion zsh > "${fpath[1]}/_cvcloud"
```

- fish

```shell
> cvcloud completion fish > ~/.config/fish/completions/cvcloud.fish
```

## 初期化
cvcloud コマンドを初めて使用する PC の場合は，以下のコマンドを実行して初期化処理をしてください．
ユーザ名は苗字になっています．

注)
- つ -> tsu
- し -> shi

```shell
$ cvcloud init --uname ユーザ名(例: iwai)
```

# 使用方法
cvcloud には以下のオブジェクトが存在します．

- machines: クラウドのインスタンスのような物
- storages: データ保存領域
- images: コンテナイメージ
- applications: jupyterlab or codeserver(vscode の Web 版) の実際にコーディングに使うアプリケーション

利用の流れは下記の通りです．

~~ 利用前 ~~

1. storages を作成する(初めて利用する場合のみ)
2. 空いている machines を探す
3. 利用する images を決める
4. applications を作成する

~~ 利用後 ~~

5. applications を削除する

## それぞれの方法について説明

### storages を作成する(初めて利用する場合のみ)
__初めて利用する場合__ storages の作成をおこないます．

ストレージが作成済みかどうかは`cvcloud get storages`コマンドで確認できます．

以下のコマンドで作成します．`--size` の後ろは半角スペースを空けて使いたい容量を入力してください．

欲しい容量は数字のみ入力してください．(例: `100GB` 欲しい場合 => `100` と入力する)

```shell
$ # storages の作成
$ cvcloud create storages --size 使いたい容量(数字のみ入力)
$ # storages が作成できているか確認する
$ cvcloud get storages
CAPACITY	STATUS	NAS DIR                                                                                                    
2000GiB 	Bound 	/share/share/cks-managed-nfs-storage/iwai-iwai-nfs-161620253-1453-pvc-foo-bar
```

NAS上 の `NAS DIR` とサーバ上の `/nas` は同期されています．

### 空いている machines を探す

`cvcloud get machines` コマンドで空いているマシンの確認や，マシンのスペック詳細を確認できます．

`cvcloud get machines --all` で他の誰かが使用中で現在使用できないマシンの詳細についても表示されます．

```shell
$ cvcloud get machines
MACHINE NAME  	CPU     	SYSTEM MEMORY	GPU TYPE	NUMBER OF GPUS	MACHINE AVAILABLE 
vram-large1   	12 Cores	64 GB        	pascal  	2             	2                	
vram-large2   	64 Cores	300 GB       	pascal  	4             	1                	
compute-midium	12 Cores	64 GB        	turing  	2             	3                	
compute-large 	30 Cores	64 GB        	ampere  	2             	1                	
```

`GPU TYPE` には GPU の世代が記載されています．pascal が一番古くて ampere が最新です．`MACHINE AVAILABLE` が machines の残量です．

### 利用する images を決める

[machinelearning-images](https://github.com/KindaiCVLAB/machinelearning-images) リポジトリで配布している機械学習用のコンテナイメージの中から選択できます．
それ以外の自前イメージも使用できますが，動作の保証はできませんし，デバックも手伝うことはできません．

__超上級者以外は基本的に配布しているものを使用してください．__

イメージについては下記のコマンドでも確認することができます．
イメージのより詳しい情報は `DETAIL` を参照してください．

```shell
$ cvcloud get images
IMAGE NAME              DETAIL                                                                 
cuda11.2.2-cudnn8       https://github.com/KindaiCVLAB/machinelearning-images#cuda1122  
cuda11.2.1-cudnn8       https://github.com/KindaiCVLAB/machinelearning-images#cuda1121  
cuda11.2.0-cudnn8       https://github.com/KindaiCVLAB/machinelearning-images#cuda1120  
cuda11.1.1-cudnn8       https://github.com/KindaiCVLAB/machinelearning-images#cuda1111  
cuda11.1-cudnn8         https://github.com/KindaiCVLAB/machinelearning-images#cuda111   
cuda11.0.3-cudnn8       https://github.com/KindaiCVLAB/machinelearning-images#cuda1103  
cuda11.0-cudnn8         https://github.com/KindaiCVLAB/machinelearning-images#cuda110   
cuda10.2-cudnn8         https://github.com/KindaiCVLAB/machinelearning-images#cuda102   
cuda10.2-cudnn7         https://github.com/KindaiCVLAB/machinelearning-images#cuda102   
```

また，`--detail` オプションを付与することで，インストールされているいくつかのライブラリバージョンを確認することもできます．

```shell
$ cvcloud get images --detail
IMAGE NAME              ANACONDA        OPENCV          TENSORFLOW-GPU          KERAS   PYTORCH         PYTORCH VISION  CUPY CUDA       STATUS     
cuda11.2.2-cudnn8       2020.11         4.5.1.48        2.6.0.dev20210411       2.4.3   1.8.1+cu111     0.9.1+cu111     8.6.0           feature         
cuda11.2.1-cudnn8       2020.11         4.5.1.48        2.6.0.dev20210411       2.4.3   1.8.1+cu111     0.9.1+cu111     8.6.0           feature         
cuda11.2.0-cudnn8       2020.11         4.5.1.48        2.6.0.dev20210411       2.4.3   1.8.1+cu111     0.9.1+cu111     8.6.0           feature         
cuda11.1.1-cudnn8       2020.11         4.5.1.48        2.4.1                   2.4.3   1.8.1+cu111     0.9.1+cu111     8.6.0           stable          
cuda11.1-cudnn8         2020.11         4.5.1.48        2.4.1                   2.4.3   1.8.0           0.9.0           8.4.0           deprecated      
cuda11.0.3-cudnn8       2020.11         4.5.1.48        2.4.1                   2.4.3   1.8.0           0.9.0           8.4.0           deprecated      
cuda11.0-cudnn8         2020.11         4.5.1.48        2.4.1                   2.4.3   1.7.1           0.8.2           8.4.0           deprecated      
cuda10.2-cudnn8         2020.11         4.5.1.48        2.4.0                   2.4.3   1.7.1           0.8.2           8.3.0           deprecated      
cuda10.2-cudnn7         2020.11         4.5.1.48        2.4.0                   2.4.3   1.7.1           0.8.2           8.3.0           deprecated      

```

### applications を作成する

アプリケーションは jupyterlab と codeserver (vscode の web ブラウザバージョン) が選べます．
それぞれ以下の特徴があります．

- jupyterlab: インタラクティブにコーディングができる
- codeserver: デバックなど機能が豊富である

`MACHINE_NAME` には手順2で決めた machines 名を，
`type` の後ろには `jupyterlab` もしくは `codeserver` を，
`IMAGE_NAME` には手順 3 で決めたイメージ名を入力してください．

```shell
$ cvcloud create applications --machines MACHINE_NAME --type [ jupyterlab | codeserver ] --images IMAGE_NAME
```

作成したら，起動するまで少し待つ必要があります．起動したか確認するには以下のコマンドを実行します．
`STATUS` が ` Running` になっていたら `LINK` に記載のURLへアクセスできます．

アクセスするとパスワードを要求されるので，`PASSWORD` 欄のパスワードを入力してください．

```shell
$ cvcloud get applications
TYPE            MACHINE         PASSWORD        STORAGE IMAGE                   LINK                            STATUS  AGE    
codeserver      compute-medium  iwai952         2000GiB cuda11.1.1-cudnn8       http://cvcloud.cks.local/iwai/  Running 24m59s  
```

### applications を削除する

リソースは限られています．__1 日でも利用しない場合は applications を削除してください．__ storages は削除する必要ありません．

その際，`cvcloud get applications` 実行時表示される `AGE` 欄で何時間起動しているか確認できるため，参考にしてください．

以下のコマンドで applications を削除します．

Note: `type` オプションを指定しなかった場合，起動している `applications` リソースが何であれ強制削除します．

```shell
$ cvcloud delete applications --type [ jupyterlab | codeserver ]
```

## その他のコマンド

### storages 容量の追加

最初に作成した storages が足りなくなった場合は，
以下のコマンドを実行することでダウンタイムなしで拡張することができます．

欲しい容量は数字のみ入力してください．(例: `100GB` 既に持っていて，更に `100GB` 欲しい場合 => `200` と入力する)

```shell
$ cvcloud update storages --size 増加後の合計容量(数字のみ)
```

### storages の削除

storages は一度作ったあと削除する必要はありませんが，もし削除したくなった場合は以下のコマンドで削除することができます．

```shell
$ cvcloud delete storages
```
