# Known Issues

## `cvcloud get images` 実行時のエラー

`cvcloud get images` 実行時稀に以下のようなエラーに遭遇することがあります．

```shell
$ cvcloud get images
GET https://api.github.com/repos/KindaiCVLAB/machinelearning-images/contents/.github/workflows?ref=master:
403 API rate limit exceeded for xxx.xxx.xxx.xxx. 
(But here's the good news: Authenticated requests get a higher rate limit. Check out the documentation for more details.) 
[rate reset in 59m59s]
```

これは，GitHub の API 利用制限に引っかかった際に出るエラーです．

### 解決策

1. エラー文に記述されている時間(上記例では 59 分 59 秒)待ってから `cvcloud get images` を実行する．
2. `cvcloud init --uname ユーザ名 --token トークン` を実行して，トークンを `cvcloud` コマンドに覚えさせる．

2 の解決策では，事前に `public_repo` の権限が付いた GitHub の personal access token を発行しておく必要があります．
発行方法は，[GitHub のドキュメント](https://docs.github.com/ja/github/authenticating-to-github/keeping-your-account-and-data-secure/creating-a-personal-access-token)
等を参考にしてください．

また，`cvcloud` コマンドへのトークンの登録はトークンの変更や有効期限切れが起こらない限り，一度行えばそれ以降実行する必要はありません．
