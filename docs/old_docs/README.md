# Create manifest for CVLAB.KubernetesService.

![cks pipline](images/cks_pipline.png "purpose of cksctl")

This automate to create kubernetes manifest.

In v2.1.3,　It is possible to create manifests for jupyterlab and PVC created from dynamic provisioning by Rook-Cephfs and NFS-Client.

However since cksctl uses Helm Chart as atemplate, you can easily increase the number of supported applications by adding new Helm Chart.

- caution
    
    cksctl is optimized for CVLAB.KubernetesService's computing performance. So, you can't use it as it is in other environment.

## how to use

I preapred docker conrainer-image.(registry.gitlab.com/yuuki_iwai/createjupyteryaml/cksctl)

If you want to know created kubernetes manifest, look at following Link.
(https://tenzen.hatenablog.com/entry/2020/03/28/203000#マニフェストの準備-1)

1. You must create directory to save setting file. (ex. setting_dir)

2. docker run!! I prepare 2 options.  

    1. You want to set new information to cksctl.(cksctl is name of this command-line-tool.)

        - docker run -it --rm -v ${HOST_SETTING_FILE_DIR}:/home/user/app/cksSettingFiles registry.gitlab.com/yuuki_iwai/createjupyteryaml/cksctl 

    2. You want use setting file.

        - docker run -it --rm -v ${HOST_SETTING_FILE_DIR}:/home/user/app/cksSettingFiles registry.gitlab.com/yuuki_iwai/createjupyteryaml/cksctl --setFname=${SET_FNAME} --setSettingsName=${SETTING_NAME} 
    
    * points to note
        - ${HOST_SETTING_FILE_DIR} is dir-path of dir that create at step1.(ex. ./setting_dir)
        - ${SET_FNAME} is setting file name that you want to use.(ex. test_settings.yaml) 
        - ${SETTING_NAME} is setting name that you want to use in ${SET_FNAME}.

## Detailed usage

Before you use cksctl, you need to prepare the following seven information.

1. The name of GitLab or GitHub account following '@'.(e.g. yuuki_iwai)

2. Access token.(if you use GitLab, you can get it by using follwing place.)

![access-token](images/publish_access_token.png "get access-token")

3. Project name in Git.

4. Username in CVLAB.KubernetesService.

5. Password to use with jupyterlab.

6. Jupyter's container image name.

7. Password to use with argocd.(default pass is same as username in cks.)

Just answer the quetion about the required computing specs.

