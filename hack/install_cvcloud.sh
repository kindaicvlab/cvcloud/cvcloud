#!/usr/bin/env bash
set -eo pipefail

release_link="https://gitlab.com/api/v4/projects/24194401/releases"
latest_version="$(curl -s $release_link | jq -r '.[0].name')"
arch="$(uname -m)"
os="$(uname)"
download_os=${os,,}

if [ "$arch" = 'x86_64' ]; then \
  download_arch="amd64";
elif [ "$arch" = 'arm64' ]; then \
  download_arch="arm64"
else \
  download_arch="arm"
fi;

latest_release_link="$release_link/$latest_version/assets/links"
download_file_name="cvcloud_${latest_version//v/}_${download_os}_${download_arch}.tar.gz"

download_url="$(curl -s "$latest_release_link" | jq -r --arg TAR "$download_file_name" '.[] | select(.name == $TAR).url')"

curl -sLO "$download_url"
tar xzf "$download_file_name"
rm -rf "$download_file_name"
chmod +x "$(pwd)/cvcloud"
echo "install to $(pwd)/cvcloud"
